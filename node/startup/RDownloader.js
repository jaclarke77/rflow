//"use strict";

const EventEmitter = require('events');
const https = require('https');
//const http = require('http');
const progress = require('progress-stream');
const fs = require('fs');
const url = require('url');
const {app} = require('electron');
const path = require('path');

const basePath = 'https://cloud.r-project.org/bin/windows/base/release.htm';

function getLatestRelease() {
  return new Promise(function (resolve, reject) {
    https.get(basePath, (res) => {
      var body = '';

      res.setEncoding('utf8');
      res.on('data', (chunk) => {
        body += chunk;
      });

      res.on('end', () => {
        var path = body.match(/<meta.*content=".*url=(.+)"/i);
        if (path) {
          resolve(url.resolve(basePath, path[1]));
        }
        else {
          reject('Latest R release could not be found');
        }
      });

    }).on('error', reject);
  });
}

class RDownloader extends EventEmitter {
  constructor() {
    super();
    this.downloadPath = '';
  }

  startDownload() {
    this.emit('queryingDownloadUrl');
    getLatestRelease().then((urlPath) => {
      this.emit('downloadStarted');

      let filename = urlPath.split('/').pop();
      let downloadDir = path.join(app.getPath('temp'), 'rflow');

      fs.mkdir(downloadDir, (err) => {
        if (err && err.code !== 'EEXIST') {
          this.emit('downloadError', err);
          return;
        }

        this.downloadPath = path.join(downloadDir, filename);

        console.log(this.downloadPath);

        var fileStream = fs.createWriteStream(this.downloadPath);

        https.get(urlPath, (res) => {
          if (res.statusCode !== 200) {
            this.emit('downloadError', res.statusCode);
            return;
          }
          var filelength = res.headers['content-length'];

          var progressStream = progress({
            length: filelength,
            time: 100
          }, (progress) => {
            this.emit('downloadProgress', progress);
            if (progress.remaining === 0) {
              this.emit('downloadFinished');
            }
          });

          res.pipe(progressStream).pipe(fileStream);

        });
      });
    }).catch((err) => {
      console.log(err);
      this.emit('downloadError', 'UrlQueryFailed');
    });
  }
}

module.exports = RDownloader;
