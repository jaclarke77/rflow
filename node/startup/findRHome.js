const path = require('path');
const glob = require('glob');

globAsync = Promise.promisify(glob);

var programLocations = [];

// windows

if (process.platform === 'win32') {
  let programFiles = ['ProgramFiles', 'ProgramFiles(x86)'];
  for (let progFilesPath of programFiles) {
    if (process.env[progFilesPath]) {
      programLocations.push(path.join( process.env[progFilesPath] ,'R', '*'));
    }
  }
}

//

function findRHome() {
  return Promise.map(programLocations, (location) => {
    return globAsync(location);
  }).then((rHomeList) => {
    return Array.prototype.concat.apply([], rHomeList);
  });
}

module.exports = findRHome;
