const execFile = require('child_process').execFile;
const path = require('path');

var execPath = {
  'win32': 'bin\\R.exe'
}[process.platform];

function getRVersion(homePath) {
  return new Promise(function(resolve, reject) {

    //console.time('getRVersion');

    execFile(path.join(homePath, execPath), ['--version'], {timeout: 10000}, (err, stdout, stderr) => {
      //console.timeEnd('getRVersion');
      if (err) {
        reject(err);
      }
      else {
        var version = stderr.match(/R\sversion\s(\d+(?:\.\d+){0,2})/i); // for some reason r seems to output version info on stderr instead of stdout
        if (version) {
          resolve(version[1]);
        }
        else {
          reject('R_executable_not_recognised');
        }
      }
    });

  });
}

module.exports = getRVersion;
