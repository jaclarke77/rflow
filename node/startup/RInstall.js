const exec = require('child_process').exec;
const arch = require('arch');

function RInstall(RInstallerPath) {
  return new Promise((resolve, reject) => {

    exec(`${RInstallerPath} /SILENT /COMPONENTS="main,${ (arch() === 'x64') ? 'x64' : 'i386' }"`, (err, stdout, stderr) => {
      if (err) reject(err);
    }).on('exit', (code) => {
      switch (code) {
        case 0:
          resolve();
          break;
        case 2:
        case 5:
          reject('install_cancelled');
          break;
        default:
          reject('installed_failed');
      }
    });

  });
}

module.exports = RInstall;
