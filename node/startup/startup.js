const settings = require('electron-settings');
const semver = require('semver');
const bytes = require('bytes');
const {app, BrowserWindow, ipcMain, dialog} = require('electron');
const createAppWindow = require('../createAppWindow.js');
const findRHome = require('./findRHome.js'),
      getRVersion = require('./getRVersion.js'),
      RDownloader = require('./RDownloader.js'),
      RInstaller = require('./RInstall.js'),
      loadBlockDefs = require('../loadBlockDefs.js');
const RSession = require('../rSession.js');
const openFile = require('../openFile.js');
const fs = require('fs');
const path = require('path');

const minRversion = '3.3.3';

var startupWindow,
    startupScreenReady = false,
    overallProgress = 0,
    progressMessage = '',
    progressDetails = '';

function updateProgress(progressBlock, progress, message, details) {
  // Progress breakdown:
  // - checking R installation :  10%
  // - loading blocks :           70%
  // - installing R packages :    20%

  if (message) progressMessage = message;

  if (typeof details === 'string') progressDetails = details;

  switch (progressBlock) {
    case 'checkR':
      overallProgress = progress * 0.1;
      break;
    case 'loadBlocks':
      overallProgress = 10 + (progress * 0.7);
      break;
    case 'instPackages':
      overallProgress = 80 + (progress * 0.2);
      break;
  }

  if (startupScreenReady && progressMessage) {
    startupWindow.webContents.send('updateProgress', {
      progress: overallProgress,
      message: progressMessage,
      details: progressDetails
    });
  }
}

ipcMain.once('startupScreenReady', () => {
  startupScreenReady = true;
  if (progressMessage) {
    startupWindow.webContents.send('updateProgress', {
      progress: overallProgress,
      message: progressMessage,
      details: progressDetails
    });
  }
});

//

function initStartup() {

  startupWindow = new BrowserWindow({
    width: 425,
    height: 170,
    frame: false,
    transparent: true,
    icon: `${__dirname}/icon.ico`,
    resizable: false,
    show: false,
    skipTaskbar: true,
    // backgroundColor: '#262626',
  });

  startupWindow.once('ready-to-show', () => {
    startupWindow.show();
    startupWindow.minimize();
    startupWindow.restore();
  });

  startupWindow.once('closed', () => {
    startupWindow = null;
  });

  startupWindow.loadURL(`file://${__dirname}/../../startup.html`);

  //startupWindow.webContents.openDevTools({mode: 'detach'});

  updateProgress('checkR', 0, 'Checking R installation');

  fs.mkdir(path.join(app.getPath('temp'), 'rflow'), (err) => {
    if (err && err.code !== 'EEXIST') {
      updateProgress('checkR', 0, 'Can\'t set up R temp directory');
      setTimeout(() => {
        startupWindow.close();
      }, 3000);
    }
    else {
      settings.get('rHomePath').then((homePath) => {
        updateProgress('checkR', 20);
        if (homePath) {
          checkRVersion(homePath).then(() => {
            startBlockLoading();
          }).catch(() => {
            settings.delete('rHomePath').then(startRsearch);
          });
        }
        else {
          startRsearch();
        }
      });
    }
  });

}

function checkRVersion(path) {
  return getRVersion(path).then((version) => {
    if (semver.gte(version, minRversion)) {
      return;
    }
    else {
      throw 'R version too old';
    }
  });
}

function startRsearch() {
  updateProgress('checkR', 20, 'Looking for R installation');
  findRHome().then((possibleHomePaths) => {
    updateProgress('checkR', 50);

    let i = 0;

    if (possibleHomePaths.length) checkVersionLoop();
    else openRNotFoundDialog();

    function checkVersionLoop() {
      checkRVersion(possibleHomePaths[i++]).then(() => {
        settings.set('rHomePath', possibleHomePaths[i-1]).then(() => {
          startBlockLoading();
        });
      }).catch(() => {
        updateProgress('checkR', 50+40*(i/possibleHomePaths.length));
        if (i === possibleHomePaths.length) openRNotFoundDialog();
        else checkVersionLoop();
      });
    }
  });
}

function openRNotFoundDialog(userSelectedR) {
  updateProgress('checkR', 90, 'Looking for R installation');
  dialog.showMessageBox(startupWindow, {
    type: 'none',
    title: 'R installation not found',
    message: (userSelectedR) ? `Selected folder not a valid R home path, or version less than v${minRversion}` : `R-Flow requires R v${minRversion} or greater installed to run.
    A valid R installation could not be found in the default location`,
    buttons: [(userSelectedR) ? 'Try again':'Let me find it', 'Install it for me', 'Cancel'],
    noLink: true
  }, (buttonId) => {
    if (buttonId === 0) {
      dialog.showOpenDialog(startupWindow, {
        title: 'Select R installation folder',
        defaultPath: process.env.ProgramFiles,
        buttonLabel: 'Select R Folder',
        properties: ['openDirectory']
      }, (directories) => {
        if (directories) {
          updateProgress('checkR', 90, 'Checking R installation');
          checkRVersion(directories[0]).then(() => {
            settings.set('rHomePath', directories[0]).then(() => {
              startBlockLoading();
            });
          }).catch(() => {
            openRNotFoundDialog(true);
          });
        }
        else openRNotFoundDialog();
      });
    }
    else if (buttonId === 1) {
      startRInstall();
    }
    else if (buttonId === 2) {
      startupWindow.close();
    }
  });
}

function startRInstall() {
  var rDownload = new RDownloader();
  var message = startupWindow.webContents;
  message.send('updateProgress', {message: 'Starting R download', querying: true});

  rDownload.on('downloadStarted', () => {
    message.send('updateProgress', {progress: 0, message: 'Downloading R'});
  }).on('downloadProgress', (progress) => {
    let byteOpts = {thousandsSeparator: ',', decimalPlaces: 1};
    message.send('updateProgress', {
      progress: progress.percentage,
      details: `${ bytes.format(progress.transferred, byteOpts) } / ${ bytes.format(progress.length, byteOpts) }`
    });
  }).on('downloadFinished', () => {
    installR(rDownload.downloadPath);
  }).on('downloadError', (err) => {
    console.log(err);
    message.send('updateProgress', {message: 'Download failed', progress: 0});
    setTimeout(() => {
      startupWindow.close();
    }, 3000);
  });

  rDownload.startDownload();
}

function installR(downloadPath) {
  var message = startupWindow.webContents;

  message.send('updateProgress', {
    message: 'Installing R',
    details: '',
    indeterminate: true,
    querying: false
  });

  RInstaller(downloadPath).then(() => {
    startRsearch();
  }).catch((err) => {
    console.log(err);
    if (err === 'install_cancelled') {
      message.send('updateProgress', {
        message: 'Install cancelled',
        progress: 0
      });
    }
    else {
      message.send('updateProgress', {
        message: 'Install failed',
        progress: 0
      });
    }
    setTimeout(() => {
      startupWindow.close();
    }, 3000);
  });
}

function startBlockLoading() {
  updateProgress('loadBlocks', 0, 'Loading blocks');
  loadBlockDefs((progress) => {
    updateProgress('loadBlocks', (progress.loaded/progress.total)*100, '',
      `${progress.loaded}/${progress.total}${(progress.failed) ? ` (${progress.failed} failed)` : ''}`
    );
  }).then(installRPackages);
}

function installRPackages() {
  updateProgress('instPackages', 0, 'Checking installed packages', '');

  let R = new RSession();

  R.run(`installed.packages()[, "Package"]`)
  .then((res) => {
    let installedPackages = res.response;

    let notInstalled = [];

    for (let package of global.packageDeps) {
      if (!installedPackages[package]) notInstalled.push(package);
    }

    if (notInstalled.length) {

      updateProgress('instPackages', 0, 'Installing R Packages');

      let failedPackages = 0;

      Promise.each(notInstalled, (package, index, length) => {
        updateProgress('instPackages', (index/length)*100, '',
          `${package} (${index+1}/${length})` + ((failedPackages) ? ` (${failedPackages} failed)` : '')
        );
        return R.run(`install.packages("${package}", quiet = TRUE)`)
        .then(() => {
          return true;
        })
        .catch((err) => {
          log.error(`[R Package Install Failed] ${err}`);
          failedPackages += 1;
          return false;
        });
      })
      .then(startMainApp);

    }
    else {
      startMainApp();
    }

  })
  .catch((err) => {
    log.error(`[R Package Check Error] ${err}`);
    startMainApp();
  });

  function startMainApp() {
    R.close();
    updateProgress('instPackages', 100, 'Starting up', '');

    createAppWindow(() => {
      require('../setAppMenu.js');
      startupWindow.close();
    })
    .then((appWindow) => {
      let inputFile = (process.argv[1] === '.') ? process.argv[2] : process.argv[1];

      if (inputFile) {
        openFile(inputFile, appWindow);
      }
    });
  }

}

module.exports = initStartup;
