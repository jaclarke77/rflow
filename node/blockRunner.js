const {BrowserWindow} = require('electron');
const ipcPromise = require('../shared/ipcPromise.js');

const convertRTypes = require('../R/convertRTypes.js');
const fixPath = require('../R/fixPath.js');

const mapValues = require('lodash/mapValues');

ipcPromise.registerListener('runBlock', (sender, blockDefId, blockId, isAdditionalFunction, details, cb) => {
  let appWindow = BrowserWindow.fromWebContents(sender);
  let windowData = global.appWindows.get(appWindow);
  let R = windowData.R;

  if (isAdditionalFunction) {
    var {additionalFunction, args} = details;
  }
  else {
    var {inputs, options} = details;
  }

  let blockDef = global.blockDefs[blockDefId];

  let functionName = (isAdditionalFunction) ? `${blockDefId}.${additionalFunction}` : blockDefId;
  let functionPath = (isAdditionalFunction) ? blockDef.additionalFunctions[additionalFunction] : blockDef.mainFunctionPath;

  if (!functionPath) {
    cb(`No function found for '${functionName}'`);
    return;
  }

  if (!windowData.loadedFunctions.includes(functionName)) {
    let deps = blockDef.requiredPackages || [];
    let loadFunctionCommand = `loadFunction("${fixPath(functionPath)}", "${functionName}", ${convertRTypes(deps)})`;

    R.run(loadFunctionCommand)
    .then(() => {
      windowData.loadedFunctions.push(functionName);
      //cb(null, result.response);
      runBlock();
    })
    .catch((err) => {
      //console.log(err);
      cb(err);
    });
  }
  else {
    runBlock();
  }

  function runBlock() {
    let mappedInputs = mapValues(inputs, (val) => {
      return {
        block: '__blockData$b'+val.blockId,
        outputId: (val.parentDataframe) ? val.outputName : (val.outputId || val.outputName),
        outputName: val.outputName,
        isDataframe: !!val.dataframe
      };
    });

    let runCommand = isAdditionalFunction ?
      `${functionName}(blockData$b${blockId}${['', ...args.map(arg => convertRTypes(arg))].join(', ')})` :
      `runBlock(${functionName}, "b${blockId}", ${convertRTypes(blockDef.fullListInput)}, ${convertRTypes(mappedInputs)}, ${convertRTypes(options)})`;

    //console.log(runCommand);
    R.run(runCommand)
      .then((result) => {
        if (result.type === "response") {
          cb(null, result.response);
        }
        else {
          cb(result);
        }
      })
      .catch((err) => {
        //console.log(err);
        cb(err);
      });
  }
});
