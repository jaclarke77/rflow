const {BrowserWindow} = require('electron');
const ipcPromise = require('../shared/ipcPromise.js');

const path = require('path');
const fs = require('fs');

ipcPromise.registerListener('getBlockTempDir', (sender, blockId, cb) => {
  let appWindow = BrowserWindow.fromWebContents(sender);
  let windowData = global.appWindows.get(appWindow);

  let tempDir = path.join(windowData.workingDir, blockId);

  fs.mkdir(tempDir, (err) => {
    if (err && err.code !== 'EEXIST') cb(`${err}`);
    else cb(null, tempDir);
  });
});
