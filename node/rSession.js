require('hazardous');

const ControlR = require('controlr');
const settings = require('electron-settings');

const rStartup = require('../R/rStartup.js');

let rHomePath = '';

settings.get('rHomePath').then((path) => {
  rHomePath = path;
});
settings.observe('rHomePath', ({newValue}) => {
  rHomePath = newValue;
});

class RSession {
  constructor(workingDir) {
    this._ready = false;
    this._busy = false;
    this._queue = [];
    this._consoleBuffer = '';
    this._controlR = new ControlR();

    let startupCmds = rStartup(workingDir);

    this._controlR.on('console', (data) => {
      this._consoleBuffer += data;
    });

    this._controlR.init({
      rhome: rHomePath,
    }).then(() => {
      //console.info('R Initialised');
      return this._controlR.internal(startupCmds);
    }).then((res) => {
      //console.info('R Ready');
      this._ready = true;
      if (this._queue.length) {
        this._runQueueItem();
      }
    }).catch((err) => {
      log.error(`[Error starting R] ${err}`);
    });
  }

  run(cmd) {
    return new Promise((resolve, reject) => {
      this._queue.push({
        cmd,
        resolve,
        reject
      });

      if (this._ready && !this._busy) {
        this._runQueueItem();
      }
    });
  }

  close() {
    return this._controlR.shutdown();
  }

  _runQueueItem() {
    this._busy = true;
    this._consoleBuffer = '';
    let queueItem = this._queue.shift();

    this._controlR.internal(queueItem.cmd).then((res) => {
      if (res.err) {
        res.errInfo = this._consoleBuffer;
        queueItem.reject(res);
      }
      else {
        queueItem.resolve(res);
      }

      if (this._queue.length) {
        this._runQueueItem();
      }
      else {
        this._busy = false;
      }
    });
  }
}

module.exports = RSession;
