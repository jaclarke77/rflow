const yazl = require('yazl');
const path = require('path');
const fs = require('fs');
const statAsync = Promise.promisify(fs.stat);

const {BrowserWindow, dialog} = require('electron');
const ipcPromise = require('../shared/ipcPromise.js');

function saveFile(appWindow, saveAs = false) {
  let windowData = global.appWindows.get(appWindow);

  if (!windowData.openFile || saveAs) {
    dialog.showSaveDialog(appWindow, {
      title: (saveAs ? 'Save File As' : 'Save File'),
      buttonLabel: (saveAs ? 'Save As' : 'Save'),
      filters: [
        {name: 'RFlow Document', extensions: ['rflow']}
      ]
    }, (filename) => {
      if (filename) {
        filepathResolved(filename);
      }
    });
  }
  else {
    filepathResolved(windowData.openFile);
  }

  function filepathResolved(filepath) {
    let zipfile = new yazl.ZipFile();

    function saveFailed(err) {
      log.error(`[Save File Failed] ${err}`);
      appWindow.send('fileSaveFailed');
    }

    zipfile.on('error', saveFailed);

    zipfile.outputStream.pipe(fs.createWriteStream(filepath+'_temp'))
    .on('close', () => {
      fs.rename(filepath+'_temp', filepath, () => {
        windowData.openFile = filepath;
        appWindow.send('fileSaved', filepath);
      });
    })
    .on('error', saveFailed);

    function getBlockgraph() {
      return ipcPromise.send(appWindow, 'getBlockgraph')
      .then((blockgraph) => {
        let blockgraphBuffer = Buffer.from(JSON.stringify(blockgraph));
        zipfile.addBuffer(
          blockgraphBuffer,
          'blockgraph'
        );
        return blockgraph;
      }).then(getPersistedFiles);
    }

    function getPersistedFiles(blockgraph) {
      let blocks = blockgraph.blocks;

      let persistedFiles = Object.keys(blocks).map((blockId) => {
        let block = blocks[blockId];
        let tempDir = global.blockDefs[block.defId].tempDir;
        return {
          blockId,
          persistFiles: tempDir && tempDir.persist
        };
      })
      .filter((block) => {
        return Array.isArray(block.persistFiles);
      })
      .reduce((a, b) => {
        return a.concat(b.persistFiles.map(filename => path.join(b.blockId, filename)));
      }, []);

      return Promise.map(persistedFiles, (filename) => {
        let filepath = path.join(windowData.workingDir, filename);
        return statAsync(filepath).then(() => {
          zipfile.addFile(filepath, filename);
        })
        .catch((err) => {
          if (!(err.code && err.code === 'ENOENT')) saveFailed(err);
        })
      });
    }

    function getRBlockData() {
      return windowData.R.run('saveBlockData()').then(() => {
        let blockDataPath = path.join(windowData.workingDir, 'RBlockData');
        zipfile.addFile(blockDataPath, 'RBlockData');
      });
    }

    Promise.join(getBlockgraph(), getRBlockData(), () => {
      zipfile.end();
    });

  }
}

module.exports = saveFile;
