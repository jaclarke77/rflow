const {app, BrowserWindow} = require('electron');
const path = require('path');
const genId = require('../shared/genid.js');
const RSession = require('./rSession.js');
const fs = require('fs');
const mkdir = Promise.promisify(fs.mkdir);

const isDevMode = process.argv.indexOf('--dev') !== -1;

function createWindow(onReadyToShow) {
  // Create the browser window.
  let win = new BrowserWindow({width: 1200, height: 650, webPreferences: {
      blinkFeatures: 'CSSBackdropFilter,CSSStickyPosition',
      webSecurity: false,
    },
    // icon: './icon.ico',
    show: false,
    frame: false
  });

  let workingDir = path.join(app.getPath('temp'), 'rflow', genId());

  let ipcQueue = [];

  win.appWindowReady = () => {
    win.send = (...args) => {
      win.webContents.send(...args);
    };

    while(ipcQueue.length) {
      win.webContents.send(...ipcQueue.shift());
    }
  };

  win.appWindowUnready = () => {
    win.send = (...args) => {
      ipcQueue.push(args);
    };
  };

  win.appWindowUnready();

  // and load the index.html of the app.

  win.loadURL(isDevMode ? 'http://localhost:8080' : `file://${__dirname}/../index.html`);

  // Install devtool extensions

  if (isDevMode) {
    const { default: installExtension, VUEJS_DEVTOOLS } = require('electron-devtools-installer');
 
    installExtension(VUEJS_DEVTOOLS)
      .then((name) => console.log(`Added Extension:  ${name}`))
      .catch((err) => console.log('An error occurred: ', err));
  }

  // Open the DevTools.
  //win.webContents.openDevTools();

  // Emitted when the window is closed.
  win.once('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    global.appWindows.get(win).R.close();

    global.appWindows.delete(win);
  });

  win.once('ready-to-show', () => {
    if (onReadyToShow) onReadyToShow();
    win.show();
  });

  return mkdir(workingDir).then(() => {
    global.appWindows.set(win, {
      workingDir,
      R: new RSession(workingDir),
      loadedFunctions: []
    });
    return win;
  });
}

module.exports = createWindow;
