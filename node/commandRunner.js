const {BrowserWindow} = require('electron');
const ipcPromise = require('../shared/ipcPromise.js');

ipcPromise.registerListener('runCommand', (sender, command, cb) => {
  let appWindow = BrowserWindow.fromWebContents(sender);
  let windowData = global.appWindows.get(appWindow);

  windowData.R.run(`${command.command}(${command.args.join(',')})`)
  .then((result) => {
    cb(null, result.response);
  })
  .catch((err) => {
    cb(err);
  });
});
