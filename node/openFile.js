const loadFile = require('./loadFile.js');
const createAppWindow = require('./createAppWindow.js');
const {dialog} = require('electron');

function openFile(filepath, appWindow) {
  if (!filepath) {
    dialog.showOpenDialog({
      title: 'Open File',
      filters: [
        {name: 'RFlow Document', extensions: ['rflow']}
      ],
      properties: ['openFile']
    }, (filepaths) => {
      if (filepaths) {
        filepath = filepaths[0];
        createWindow();
      }
    });
  }
  else createWindow();

  function createWindow() {
    if (!appWindow) {
      createAppWindow().then(envirReady);
    }
    else {
      envirReady(appWindow);
    }
  }

  function envirReady(appWindow) {

    let windowData = global.appWindows.get(appWindow);
    let {R, workingDir} = windowData;

    appWindow.send('fileLoadingStarted', filepath);

    loadFile(filepath, workingDir).then((file) => {
      appWindow.send('loadBlockgraph', file.blockgraph);

      return R.run('loadBlockData()').then(() => {
        windowData.openFile = filepath;
        appWindow.send('fileLoadingFinished');
      });
    })
    .catch((err) => {
      if (err.type) log.error(`[Open File Failed] ${err.type} ${err.err}`);
      else log.error(`[Open File Failed] ${err}`);

      appWindow.send('fileLoadingFailed', err);
    });

  }

}

module.exports = openFile;
