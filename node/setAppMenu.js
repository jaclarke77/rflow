const {BrowserWindow, Menu, ipcMain: ipc} = require('electron');

const createAppWindow = require('./createAppWindow.js');
const openFile = require('./openFile.js');
const saveFile = require('./saveFile.js');

const menuEvents = {
  newDocument() {
    createAppWindow();
  },
  openFile() {
    openFile();
  },
  saveFile(appWindow) {
    saveFile(appWindow);
  },
  saveFileAs(appWindow) {
    saveFile(appWindow, true);
  },
  reloadWindow(appWindow) {
    appWindow.reload();
    let openFilePath = global.appWindows.get(appWindow).openFile;
    if (openFilePath) {
      setTimeout(() => {
        openFile(openFilePath, appWindow);
      }, 1000);
    }
  },
  toggleDevTools(appWindow) {
    appWindow.webContents.toggleDevTools();
  }
};

function onClick(item, appWindow, event) {
  menuEvents[item.id](appWindow);
}

ipc.on('menuItemClick', (event, id) => {
  let appWindow = BrowserWindow.fromWebContents(event.sender);
  menuEvents[id](appWindow);
});

const template = require('../shared/menuTemplate.js')(onClick);

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);
