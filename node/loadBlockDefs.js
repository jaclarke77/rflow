const fs = Promise.promisifyAll(require('fs')),
      path = require('path'),
      { app } = require('electron'),
      Ajv = require('ajv'),
      blockDefSchema = require('./schemas/blockdef.json');

const ajv = new Ajv({
  v5: true,
  useDefaults: true,
  schemas: [
    require('./schemas/socketItem.json'),
    require('./schemas/optionItem.json'),
    require('./schemas/outputs.json')
  ]
});

require('ajv-merge-patch')(ajv);

const validator = ajv.compile(blockDefSchema);

let appResDir = app.getAppPath();
if (appResDir.endsWith('.asar')) appResDir = path.join(appResDir, '..');

const blockDefsDir = path.join(appResDir, 'block_defs');

global.blockDefs = {};
global.packageDeps = ['purrr'];

function loadBlockDefs(progressUpdate) {
  let totalBlocks = 0,
      loadedBlocks = 0,
      failedBlocks = 0;

  return fs.readdirAsync(blockDefsDir)
  .then((blocks) => {
    totalBlocks = blocks.length;
    return blocks;
  })
  .map((blockId) => {
    return loadBlock(blockId).then((failed) => {
      loadedBlocks += 1;
      if (failed) failedBlocks += 1;
      progressUpdate({
        total: totalBlocks,
        loaded: loadedBlocks,
        failed: failedBlocks
      });
      return;
    });
  }, {concurrency: 20})
  .then(() => {
    return;
  });
}

function loadBlock(blockId) {
  return new Promise((resolve, reject) => {
    if (/^[a-z0-9.]+$/.test(blockId)) {

      fs.readFileAsync(path.join(blockDefsDir, blockId, 'config.json'), 'utf8')
      .then((fileContent) => {
        let config = JSON.parse(fileContent);

        if (validator(config)) {
          return config;
        }
        else {
          throw {name: 'BLOCK_CONFIG_INVALID', message: JSON.stringify(validator.errors)};
        }
      })
      .then((config) => {
        if (config.tempDir && (config.tempDir.persist && !config.tempDir.required) ) {
          throw {name: 'BLOCK_CONFIG_TEMPDIR_INVALID', message: 'Cannot have persisted temp files without the temp dir required'};
        }
        else {
          return config;
        }
      })
      .then((config) => {
        let functionPath = path.join(blockDefsDir, blockId, 'main.R');
        return fs.accessAsync(functionPath, fs.constants.R_OK)
        .then(() => {
          config.mainFunctionPath = path.resolve(functionPath);
          return config;
        })
        .catch((err) => {
          throw {name: 'BLOCK_R_FUNCTION_NOT_FOUND', message: 'No main.R file found'};
        });
      })
      .then((config) => {
        if (config.options === 'custom') {
          let customOptionsPath = path.join(blockDefsDir, blockId, 'main.js');
          return fs.accessAsync(customOptionsPath, fs.constants.R_OK)
          .then(() => {
            config.options = {
              customPath: path.resolve(customOptionsPath)
            };
            return config;
          })
          .catch((err) => {
            throw {name: 'BLOCK_CUSTOM_OPTIONS', message: 'Custom options specified but no main.js file found'};
          });
        }
        else return config;
      })
      .then((config) => {
        if (config.additionalFunctions) {
          for (let functionName of Object.keys(config.additionalFunctions)) {
            let functionPath = config.additionalFunctions[functionName];
            functionPath = path.resolve(path.join(blockDefsDir, blockId, functionPath));
            config.additionalFunctions[functionName] = functionPath;
          }
        }
        return config;
      })
      .then((config) => {
        if (config.requiredPackages) {
          for (let packageName of config.requiredPackages) {
            if (!packageDeps.includes(packageName)) packageDeps.push(packageName);
          }
        }
        return config;
      })
      .then((config) => {
        if (config.inputs && Array.isArray(config.inputs)) {
          config.inputs = config.inputs.map(input => {
            if (!Array.isArray(input.type)) input.type = [input.type];
            return input;
          })
        }
        blockDefs[blockId] = config;
        resolve();
      })
      .catch((err) => {
        if (err.code && err.code === 'ENOENT') {
          logBlockError('BLOCK_LOAD_ERROR', blockId, 'No config.json file found');
        }
        else {
          logBlockError(err.name, blockId, err.message);
        }

        resolve(true);
      });
    }
    else {
      logBlockError('INVALID_BLOCKID', blockId, '');
      resolve(true);
    }
  });
}

function logBlockError(blockError, blockId, error) {
  log.error(`[${blockError}] <${blockId}> ${error}`);
}

module.exports = loadBlockDefs;
