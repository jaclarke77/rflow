const yauzl = require('yauzl');
const getRawBody = Promise.promisify(require('raw-body'));

const openZip = Promise.promisify(yauzl.open);

const fs = require('fs');
const path = require('path');

const mkdirAsync = Promise.promisify(fs.mkdir);

const Handlers = {
  '^blockgraph$': function(workingDir, filename, readStream) {
    return getRawBody(readStream, 'utf8')
      .then((res) => {
        try {
          return JSON.parse(res);
        }
        catch(err) {
          throw {type: 'FILE_CORRUPT', err: `Error parsing 'blockgraph': ${err}`};
        }
      });
  },
  '^RBlockData$': function(workingDir, filename, readStream) {
    let filepath = path.join(workingDir, 'RBlockData');
    return copyReadstreamToFile(readStream, filepath);
  },
  '^[0-9A-Za-z]{21,22}[\\/\\\\].+$': function(workingDir, filename, readStream) {
    let dirpath = filename.split(/\/|\\/)[0];
    mkdirAsync(path.join(workingDir, dirpath))
    .then(() => {
      return copyReadstreamToFile(readStream, path.join(workingDir, filename));
    })
    .catch((err) => {
      if (err.code !== 'EEXIST') throw err;
    });
  }
};

const requiredFiles = ['blockgraph', 'RBlockData'];

function copyReadstreamToFile(readStream, filepath) {
  let writeStream = fs.createWriteStream(filepath);
  readStream.on('error', () => {
    writeStream.end();
  });
  return new Promise((resolve) => {
    writeStream.on('finish', () => {
      resolve(filepath);
    });
    readStream.pipe(writeStream);
  });
}

function findHandler(filename) {
  let handlerId = Object.keys(Handlers).find((handlerId) => {
    return filename.search(handlerId) !== -1;
  });
  return Handlers[handlerId];
}

function loadFile(filename, workingDir) {
  if (path.extname(filename) !== '.rflow') {
    return Promise.reject({type: 'NOTRFLOW', err: 'File is not a .rflow file'});
  }

  return openZip(filename, {autoClose: true, lazyEntries: true})
  .then((zipfile) => {
    function getEntryReadStream(entry) {
      return new Promise((resolve, reject) => {
        zipfile.openReadStream(entry, (err, readStream) => {
          if (err) reject({type: 'FILE_ERROR', err: `Error opening part stream (${entry.fileName}): ${err}`});
          else {
            readStream.on('error', (err) => {
              reject({type: 'FILE_CORRUPT', err: `Error reading part stream (${entry.fileName}): ${err}`});
            });
            resolve(readStream);
          }
        });
      });
    }

    return new Promise((resolve, reject) => {
      let results = {};

      zipfile
      .on('entry', (entry) => {
        let handler = findHandler(entry.fileName);
        if (handler) {
          getEntryReadStream(entry)
          .then((readStream) => {
            return handler(workingDir, entry.fileName, readStream);
          })
          .then((result) => {
            results[entry.fileName] = result;
            zipfile.readEntry();
          })
          .catch((err) => {
            if (err.type) reject(err);
            reject({type: 'FILE_CORRUPT', err: `Error processing file part (${entry.fileName}): ${err}`});
          });
        }
        else {
          log.warn(`[LOAD_FILE] No handler found for part '${entry.fileName}' in '${filename}'`);
          zipfile.readEntry();
        }
      })
      .on('end', () => {
        for (let file of requiredFiles) {
          if (!results[file]) reject({type: 'FILE_CORRUPT', err: `Required part (${file}) missing from file`});
        }
        resolve(results);
      })
      .on('error', (err) => {
        reject({type: 'FILE_UNKNOWN_ERR', err: `${err}`});
      });

      zipfile.readEntry();
    });
  })
  .catch((err) => {
    if (err.type) throw err;
    throw {type: 'FILE_CORRUPT', err: `${err}`};
  });
}

module.exports = loadFile;
