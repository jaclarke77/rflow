require('hazardous');

const {app} = require('electron');
const path = require('path');
const fs = require('fs');

function repSlash(path) {
  return path.replace(/\\/g, '/');
}

let RLibsPath = path.join(app.getPath('userData'), 'RLibrary');

try {
  fs.mkdirSync(RLibsPath);
}
catch(err) {
  if (err && err.code !== 'EEXIST') log.error(`${err}`);
}

let cmds = [
  // packages setup
  `options(repos = c(CRAN = "https://cloud.r-project.org/"))`,
  `.libPaths("${repSlash(RLibsPath)}")`,
  // load core rflow functions
  `source("${repSlash(path.resolve(__dirname, 'rflow_core.R'))}")`
];

module.exports = function(workingDir) {
  let startupCmds = [...cmds];

  if (workingDir) {
    startupCmds.push(`setwd("${repSlash(workingDir)}")`);
  }

  //console.log(startupCmds);

  return startupCmds;
};
