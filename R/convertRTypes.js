const fixPath = require('./fixPath.js');

function convertRTypes(val) {
  if (val instanceof Array) {
    let isVector = true;
    const valList = val.map((value) => {
      if (!['number', 'string', 'boolean'].includes(typeof value)) isVector = false;
      return convertRTypes(value)
    })
    return `${isVector?'c':'list'}(${valList.join(', ')})`;
  }
  if (typeof val === 'object') {
    return `list(${
      Object.keys(val)
      .map((key) => `"${key}" = ${convertRTypes(val[key])}`)
      .join(', ')
    })`;
  }
  if (typeof val === 'number') {
    return val;
  }
  if (typeof val === 'boolean') {
    return val ? 'TRUE' : 'FALSE';
  }
  let valStr = String(val);
  if (valStr.startsWith('__')) return `${valStr.substr(2)}`;
  if (valStr.startsWith('//')) return `"${fixPath(valStr.substr(2))}"`;
  return `"${valStr}"`;
}

module.exports = convertRTypes;
