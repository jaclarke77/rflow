const isRenderer = (typeof window !== 'undefined');

const ipc = isRenderer ? require('electron').ipcRenderer : require('electron').ipcMain;

const messageQueue = new Map();
let currentMessageId = 0;
const listeners = new Map();

function registerListener(channel, listener) {
  listeners.set(channel, listener);
}

function removeListener(channel) {
  listeners.delete(channel);
}

ipc.on('ipcPromise', (event, messageId, channel, ...args) => {
  let listener = listeners.get(channel);
  if (listener) {
    listener(event.sender, ...args, (err, ...returnArgs) => {
      event.sender.send('ipcPromiseReturn', messageId, err, ...returnArgs);
    });
  }
});

ipc.on('ipcPromiseReturn', (event, messageId, err, ...returnArgs) => {
  if (messageQueue.has(messageId)) {
    let {resolve, reject} = messageQueue.get(messageId);
    messageQueue.delete(messageId);
    if (err) {
      reject(err, ...returnArgs);
    }
    else resolve(...returnArgs);
  }
});

function sendPromise(messageId) {
  return new Promise((resolve, reject) => {
    messageQueue.set(messageId, {resolve, reject});
  });
}

const send = isRenderer ?
  function(channel, ...args) {
    let messageId = currentMessageId++;
    ipc.send('ipcPromise', messageId, channel, ...args);
    return sendPromise(messageId);
  } :
  function(appWindow, channel, ...args) {
    let messageId = currentMessageId++;
    appWindow.send('ipcPromise', messageId, channel, ...args);
    return sendPromise(messageId);
  };

module.exports = { send, registerListener, removeListener };
