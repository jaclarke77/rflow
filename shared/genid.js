const uuid = require('node-uuid'),
      basex = require('base-x');

const b62 = basex('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');

module.exports = function genId() {
  let uuidArray = [];
  uuid.v4(null, uuidArray);
  return b62.encode(uuidArray);
};
