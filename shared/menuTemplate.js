module.exports = function(onClick) {
  return [
    {
      label: 'File',
      submenu: [
        {
          id: 'newDocument',
          label: 'New',
          accelerator: 'CmdOrCtrl+N',
          click: onClick
        },
        {
          id: 'openFile',
          label: 'Open',
          accelerator: 'CmdOrCtrl+O',
          click: onClick
        },
        {
          id: 'saveFile',
          label: 'Save',
          accelerator: 'CmdOrCtrl+S',
          click: onClick
        },
        {
          id: 'saveFileAs',
          label: 'Save As',
          accelerator: 'CmdOrCtrl+Shift+S',
          click: onClick
        }
      ]
    },
    {
      label: 'Dev',
      submenu: [
        {
          id: 'reloadWindow',
          label: 'Reload',
          accelerator: 'CmdOrCtrl+R',
          click: onClick
        },
        {
          id: 'toggleDevTools',
          label: 'Toggle Developer Tools',
          accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
          click: onClick
        }
      ]
    }
  ];
};
