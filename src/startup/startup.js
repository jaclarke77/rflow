import Vue from 'vue';
import startup from './startup.vue';

new Vue({
  el: '#startup',
  render: h => h(startup)
});
