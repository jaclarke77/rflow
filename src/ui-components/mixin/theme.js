export default {
  props: {
    theme: {
      type: String,
      validator(value) {
        return ['light', 'dark', 'primary', 'accent'].indexOf(value) !== -1;
      }
    }
  }
};
