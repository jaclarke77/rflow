const ipcPromise = require('../../shared/ipcPromise.js');

export function runCommand(command) {
  return ipcPromise.send('runCommand', command);
}
