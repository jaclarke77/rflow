import Quill from 'quill/core';

import Toolbar from 'quill/modules/toolbar';
import Bubble from 'quill/themes/bubble';

import Bold from 'quill/formats/bold';
import Italic from 'quill/formats/italic';
import Header from 'quill/formats/header';
import Strike from 'quill/formats/strike';
import Underline from 'quill/formats/underline';
import List, { ListItem } from 'quill/formats/list';
import Script from 'quill/formats/script';

Quill.register({
  'modules/toolbar': Toolbar,
  'themes/bubble': Bubble,
  'formats/bold': Bold,
  'formats/italic': Italic,
  'formats/header': Header,
  'formats/strike': Strike,
  'formats/underline': Underline,
  'formats/list': List,
  'formats/list/item': ListItem,
  'formats/script': Script,
});

export { Quill };

export const quillOptions = {
  theme: 'bubble',
  placeholder: 'Add note...',
  formats: [
    'bold',
    'italic',
    'strike',
    'script',
    'underline',
    'header',
    'list',
  ],
  modules: {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'header': 1 }, { 'header': 2 }],
      [{ 'list': 'ordered'}, { 'list': 'bullet' }],
      [{ 'script': 'sub'}, { 'script': 'super' }],
    ],
  }
}
