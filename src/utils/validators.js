export default {
  required(value) {
    if (Array.isArray(value)) return !!value.length;
    return (value === undefined || value === null) ? false : !!String(value).length;
  },
  alphanumeric(value) {
    return /^[a-zA-Z0-9 ]*$/.test(value);
  },
  minLength(value, min) {
    return value.length >= min;
  },
};
