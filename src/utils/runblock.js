const ipcPromise = require('../../shared/ipcPromise.js');

export function runBlock({defId, blockId, inputs, options}) {
  return ipcPromise.send('runBlock', defId, blockId, false, {inputs, options});
    // .then((...res) => {
    //   console.log('res', ...res);
    // })
    // .catch((...err) => {
    //   console.log('err', ...err);
    // });
}
