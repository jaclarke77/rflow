import dragStart from './drag.js';
import store from '../vuex/store.js';

const threshold = 20,
      lim = 25;

var screenSize,
    canvasTotalDelta,
    updateHandler = null;

export default function autoscrollDragStart(e, update, end, cursor) {
  updateHandler = update;
  screenSize = store.getters.screenSize;
  canvasTotalDelta = {
    x: 0,
    y: 0
  };

  dragStart(e, dragUpdate, end, true, cursor);
}

function dragUpdate(mouseData) {
  let offset = {
    x: 0,
    y: 0
  },
  canvasDelta = {
    x: 0,
    y: 0
  },
  mousePos = mouseData.mousePos;

  if (mousePos.x < threshold) {
    offset.x = mousePos.x-threshold;
  }
  if (mousePos.y < threshold) {
    offset.y = mousePos.y-threshold;
  }
  if (mousePos.x > (screenSize.width-threshold)) {
    offset.x = mousePos.x - (screenSize.width-threshold);
  }
  if (mousePos.y > (screenSize.height-threshold)) {
    offset.y = mousePos.y - (screenSize.height-threshold);
  }

  if (offset.x || offset.y) {
    let initialCanvasPos = {...store.getters.canvasPos};
    store.dispatch('canvasMoveTo', {
      pos: {
        x: limit(-offset.x/2),
        y: limit(-offset.y/2)
      },
      silent: true,
      relative: true
    });

    canvasDelta = {
      x: store.getters.canvasPos.x - initialCanvasPos.x,
      y: store.getters.canvasPos.y - initialCanvasPos.y,
    };

    canvasTotalDelta.x += canvasDelta.x;
    canvasTotalDelta.y += canvasDelta.y;
  }

  if (canvasDelta.x || canvasDelta.y || mouseData.mouseDelta.x || mouseData.mouseDelta.y) {
    updateHandler({
      canvasDelta,
      canvasTotalDelta,
      ...mouseData
    });
  }
}

function limit(val) {
  return (val > lim) ? lim : ((val < -lim) ? -lim : val);
}
