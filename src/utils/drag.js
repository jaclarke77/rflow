import store from '../vuex/store.js';

var mousePos = {
    x: 0,
    y: 0
  },
  initialMousePos = {
    x: 0,
    y: 0
  },
  prevFrame = {
    mousePos: {
      x: 0,
      y: 0
    },
    timestamp: 0
  },
  mouseVelocity = {
    x: 0,
    y: 0
  },
  isDragging = false,
  continuousUpdate = false,
  updateHandler = null,
  endHandler = null;

export default function dragStart(e, update, end, continuous = false, cursor = 'default') {
  isDragging = true;
  updateHandler = update;
  endHandler = end;
  continuousUpdate = continuous;
  window.addEventListener('mousemove', dragMove);
  window.addEventListener('mouseup', dragEnd);
  document.body.style['pointer-events'] = 'none';
  document.body.parentElement.style.cursor = cursor;
  store.dispatch('setGlobalDragging', true);
  mouseVelocity = {
    x: 0,
    y: 0
  };
  dragMove(e);
  initialMousePos = mousePos;
  prevFrame.mousePos = mousePos;
  prevFrame.timestamp = performance.now();

  requestAnimationFrame(dragLoop);
}

function dragMove(e) {
  e.preventDefault(); // not sure what action this is preventing, but seems to be needed to let right click dragging work past the edge of the window
  mousePos = {
    x: e.clientX,
    y: e.clientY
  };
}

function dragLoop(timestamp) {
  if (isDragging) {
    var tDelta = timestamp - prevFrame.timestamp;
    prevFrame.timestamp = timestamp;

    var mouseDelta = {
      x: mousePos.x - prevFrame.mousePos.x,
      y: mousePos.y - prevFrame.mousePos.y
    };
    prevFrame.mousePos = mousePos;

    var mouseTotalDelta = {
      x: mousePos.x - initialMousePos.x,
      y: mousePos.y - initialMousePos.y
    };

    mouseVelocity = { // set velocity to smoothed mouse velocity
      x: 0.3*mouseVelocity.x + 0.7*((mouseDelta.x*1000)/(1+tDelta)),
      y: 0.3*mouseVelocity.y + 0.7*((mouseDelta.y*1000)/(1+tDelta))
    };

    if (continuousUpdate || mouseDelta.x || mouseDelta.y) {
      updateHandler({
        mouseDelta,
        mouseTotalDelta,
        mousePos
      });
    }

    requestAnimationFrame(dragLoop);
  }
}

function dragEnd() {
  isDragging = false;
  window.removeEventListener('mousemove', dragMove);
  window.removeEventListener('mouseup', dragEnd);
  document.body.style['pointer-events'] = 'auto';
  document.body.parentElement.style.cursor = 'auto';
  store.dispatch('setGlobalDragging', false);

  endHandler({mouseVelocity});
}
