import Vue from 'vue';
import PortalVue from 'portal-vue';

import App from './App.vue';

import uiButton from './ui-components/button.vue';
import textInput from './ui-components/textinput.vue';
import uiSelect from './ui-components/select.vue';
import multiSelect from './ui-components/multiselect.vue';
import boolean from './ui-components/boolean.vue';

Vue.use(PortalVue);

let globalComponents = {
  'ui-button': uiButton,
  textinput: textInput,
  'ui-select': uiSelect,
  multiselect: multiSelect,
  boolean
};

for (let componentName of Object.keys(globalComponents)) {
  Vue.component(componentName, globalComponents[componentName]);
}

let app = new Vue({
  el: '#app',
  render: h => h(App)
});

window.onbeforeunload = () => {
  app.$destroy();
};
