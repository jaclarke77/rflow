export function round(val, dir, to = 40) {
  switch (dir) {
    case 'up':
      return Math.ceil(val/to)*to;
    case 'down':
      return Math.floor(val/to)*to;
    default:
      return Math.round(val/to)*to;
  }
}

/*
 * https://gist.github.com/gre/1650294
 * Easing Functions - inspired from http://gizma.com/easing/
 * only considering the t value for the range [0, 1] => [0, 1]
 */
const EasingFunctions = {
  // no easing, no acceleration
  linear: function (t) { return t; },
  // accelerating from zero velocity
  easeInQuad: function (t) { return t*t; },
  // decelerating to zero velocity
  easeOutQuad: function (t) { return t*(2-t); },
  // acceleration until halfway, then deceleration
  easeInOutQuad: function (t) { return t<0.5 ? 2*t*t : -1+(4-2*t)*t; },
  // accelerating from zero velocity
  easeInCubic: function (t) { return t*t*t; },
  // decelerating to zero velocity
  easeOutCubic: function (t) { return (--t)*t*t+1; },
  // acceleration until halfway, then deceleration
  easeInOutCubic: function (t) { return t<0.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1; },
  // accelerating from zero velocity
  easeInQuart: function (t) { return t*t*t*t; },
  // decelerating to zero velocity
  easeOutQuart: function (t) { return 1-(--t)*t*t*t; },
  // acceleration until halfway, then deceleration
  easeInOutQuart: function (t) { return t<0.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t; },
  // accelerating from zero velocity
  easeInQuint: function (t) { return t*t*t*t*t; },
  // decelerating to zero velocity
  easeOutQuint: function (t) { return 1+(--t)*t*t*t*t; },
  // acceleration until halfway, then deceleration
  easeInOutQuint: function (t) { return t<0.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t; },
  // exponential out
  expoOut: function (t) {
    return 1 - Math.pow(2, -10*t);
  }
};

var tweens = new Map();
var allTweensEndedCallbacks = [];

export const tween = {
  start(id, {type = 'linear', duration = 200, from, to}, step, end) {
    var values;
    if (to) {
      values = {
        type: '',
        from: {},
        dist: {}
      };
      if (typeof to === 'number') {
        values.type = 'number';
        values.from.number = (from && typeof from === 'number') ? from : 0;
        values.dist.number = to - values.from.number;
      }
      else if (typeof to === 'object') {
        values.type = (Array.isArray(to)) ? 'array': 'object';
        for (var key in to) {
          if (to.hasOwnProperty(key) && typeof to[key] === 'number') {
            values.from[key] = (from && from[key] && typeof from[key] === 'number') ? from[key] : 0;
            values.dist[key] = to[key] - values.from[key];
          }
        }
      }
    }

    tweens.set(id, {
      type,
      duration,
      values,
      step,
      end,
      initialTimestamp: 0
    });
    if (tweens.size === 1) requestAnimationFrame(_tweenLoop);
  },
  cancel(id) {
    if (tweens.has(id)) tweens.delete(id);
  },
  allEnded(callback) {
    if (tweens.size) {
      allTweensEndedCallbacks.push(callback);
    }
    else {
      callback();
    }
  }
};

function _tweenLoop(timestamp) {
  for (var [id, tween] of tweens) {
    if (!tween.initialTimestamp) tween.initialTimestamp = timestamp;
    let tDelta = timestamp - tween.initialTimestamp,
    tProgress = Math.min(tDelta/tween.duration, 1),
    progress = EasingFunctions[tween.type](tProgress),
    value;

    if (tween.values) {
      value = (tween.values.type === 'array') ? [] : {};
      for (var key in tween.values.from) {
        if (tween.values.from.hasOwnProperty(key)) {
          value[key] = tween.values.from[key] + tween.values.dist[key] * progress;
        }
      }
      if (tween.values.type === 'number') value = value.number;
    }

    tween.step({progress, value});

    if (tProgress === 1) {
      if (tween.end) tween.end();
      tweens.delete(id);
    }
  }

  if (tweens.size) {
    requestAnimationFrame(_tweenLoop);
  }
  else {
    while (allTweensEndedCallbacks.length) {
      allTweensEndedCallbacks.pop()();
    }
  }
}

export function getBlocksPipes(block) {
  const socketTypes = ['insockets', 'outsockets'];
  var pipes = [];

  for (let socketType of socketTypes) {
    let sockets = block[socketType];
    for (let socketId of Object.keys(sockets)) {
      let socket = sockets[socketId];
      pipes.push(...Object.keys(socket.pipes));
    }
  }

  return pipes;
}

export function getBlockLineage(store, type, blockId) {
  var blocks = store.getters.blocks,
      pipes = store.getters.pipes,
      lineage = [],
      unchecked = [blockId];

  while (unchecked.length) {
    let block = blocks[unchecked.shift()],
        blockSockets = block[(type === 'ancestors') ? 'insockets' : 'outsockets'];

    for (let socketId of Object.keys(blockSockets)) {
      let socket = blockSockets[socketId];
      for (let pipeId of Object.keys(socket.pipes)) {
        let uncheckedBlockId = pipes[pipeId][(type === 'ancestors') ? 'start' : 'end'].block;
        if (uncheckedBlockId && !lineage.includes(uncheckedBlockId)) {
          lineage.push(uncheckedBlockId);
          unchecked.push(uncheckedBlockId);
        }
      }
    }
  }

  return lineage;
}

export function getPlugYPos(store, pipeId, plugType) {
  var plug = store.getters.pipes[pipeId][plugType];

  if (!plug.block) {
    return plug.pos.y;
  }

  var block = store.getters.blocks[plug.block];

  return block.pos.y + block[(plugType === 'start') ? 'outsockets' : 'insockets'][plug.socket].ypos;
}
