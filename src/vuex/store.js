import Vue from 'vue';
import Vuex from 'vuex';

import global from './modules/global/global.js';
import blockGraph from './modules/blockgraph/blockgraph.js';
import canvas from './modules/canvas/canvas.js';

Vue.use(Vuex);

const store = new Vuex.Store({
  modules: {
    global,
    blockGraph,
    canvas
  },
  strict: process.env.NODE_ENV !== 'production'
});

store.watch(()=>store.getters.blockBoundingBox, function(newVal, oldVal) {
  const xdiff = newVal.left - oldVal.left;
  const ydiff = newVal.top - oldVal.top;
  if (xdiff || ydiff) {
    store.dispatch('canvasMoveTo', {
      pos: {
        x: xdiff/2,
        y: ydiff/2,
      },
      relative: true,
    });
  }
});

export default store;
