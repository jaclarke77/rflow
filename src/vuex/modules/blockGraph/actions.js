const {remote} = window.require('electron');

import blockActions from './actions/blockActions.js';
import pipeActions from './actions/pipeActions.js';
import socketActions from './actions/socketActions.js';
import noteActions from './actions/noteActions.js';

import {runBlock} from '../../../utils/runblock.js';
import {runCommand} from '../../../utils/runcommand.js';

export default {

  ...blockActions,
  ...pipeActions,
  ...socketActions,
  ...noteActions,

  // blockgraph

  loadBlockGraphData(store, blockgraph) {
    for (let blockId of Object.keys(blockgraph.blocks)) {
      store.dispatch('addBlock', {
        id: blockId,
        ...blockgraph.blocks[blockId]
      });
    }
    for (let pipeId of Object.keys(blockgraph.pipes)) {
      store.dispatch('addPipe', {
        id: pipeId,
        pipe: blockgraph.pipes[pipeId],
        fileLoading: true
      });
    }
    for (let note of blockgraph.notes) {
      store.commit('ADD_NOTE', {
        blocks: note.blockIds.map(blockId => store.getters.blocks[blockId]),
        preview: note.preview,
        contents: note.contents,
      });
    }

    store.commit('SET_UNSAVED', false);

    setTimeout(() => {store.dispatch('canvasMoveToCenter');}, 0);
  },

  getBlockGraphData(store) {
    let blocks = store.getters.blocks,
        pipes = store.getters.pipes,
        notes = store.getters.notes,
        blockgraph = {
          blocks: {},
          pipes: {},
          notes: [],
        };

    store.dispatch('compactBlockOrder');

    for (let blockId of Object.keys(blocks)) {
      let block = blocks[blockId];
      let blockDef = remote.getGlobal('blockDefs')[block.defId];
      blockgraph.blocks[blockId] = {
        defId: block.defId,
        pos: block.pos,
        size: block.size,
        order: block.order,
        optionsData: block.optionsData,
        dataValid: block.dataValid
      };

      if (Object.keys(block.persistState).length) {
        blockgraph.blocks[blockId].persistState = block.persistState;
      }

      if (block.multiInputs) {
        blockgraph.blocks[blockId].multiInputs = block.multiInputs;
      }

      let dataframes = {};
      block.outsocketOrder.filter((id) => {
        return block.outsockets[id].parentDataframe;
      }).forEach((id) => {
        let socket = block.outsockets[id],
            parent = socket.parentDataframe;
        if (!dataframes[parent]) dataframes[parent] = [];
        dataframes[parent].push({
          id: id.split('$')[1],
          name: socket.name,
          type: socket.type
        });
      });
      if (Object.keys(dataframes).length) blockgraph.blocks[blockId].dataframes = dataframes;

      if (blockDef.inputs === 'custom') {
        blockgraph.blocks[blockId].customInputs = block.insocketOrder.map((id) => {
          let socket = block.insockets[id];
          return {
            id,
            name: socket.name,
            type: socket.type,
            optional: socket.optional
          };
        });
      }
      if (blockDef.outputs && blockDef.outputs.type === 'list') {
        let renamedOutputs = block.outsocketOrder.reduce((outputs, id, index) => {
          let socket = block.outsockets[id];
          if (blockDef.outputs.items[index].name !== socket.name) outputs[id] = socket.name;
          return outputs;
        }, {});
        if (Object.keys(renamedOutputs).length) blockgraph.blocks[blockId].renamedOutputs = renamedOutputs;
      }
    }

    for (let pipeId of Object.keys(pipes)) {
      let pipe = pipes[pipeId];
      blockgraph.pipes[pipeId] = {};
      for (let type of ['start', 'end']) {
        blockgraph.pipes[pipeId][type] = pipe[type].block ? {
          block: pipe[type].block,
          socket: pipe[type].socket
        } : {
          pos: pipe[type].pos
        };
      }
    }

    blockgraph.notes = notes.map((note) => {
      let blockIds = note.blocks.map(block => block.id);
      return {
        blockIds,
        preview: note.preview,
        contents: note.contents,
      }
    })

    return blockgraph;
  },

  // options data

  updateOptionsData(store, args) {
    store.commit('UPDATE_OPTIONS_DATA', args);
    store.commit('SET_UNSAVED');
  },

  updatePersistState(store, args) {
    store.commit('UPDATE_PERSIST_STATE', args);
    store.commit('SET_UNSAVED');
  },

  // block data

  invalidateBlockData(store, {blockId}) {
    store.dispatch('cancelBlockDataProcessing', {blockId});
    store.commit('UPDATE_BLOCK_DATA_VALIDITY', {blockId, validity: false});
    store.commit('SET_BLOCK_DATA_ERROR', {blockId, error: ''});
  },

  processBlockData(store, {blockId, inputs}) {
    store.commit('SET_BLOCK_PROCESS_RUNNING', {blockId, running: true});

    let block = store.getters.blocks[blockId],
        defId = block.defId,
        options = block.optionsData;

    return runBlock({
      defId,
      blockId,
      inputs,
      options
    })
    .then((result) => {
      if (block.processRunning) {
        store.commit('SET_BLOCK_PROCESS_RUNNING', {blockId, running: false});

        store.dispatch('updateBlockOutputs', {blockId, outputInfo: result.$data});

        store.commit('UPDATE_BLOCK_DATA_VALIDITY', {blockId, validity: true});

        return true;
      }
    })
    .catch((err) => {
      if (block.processRunning) {
        console.log(err);
        store.commit('SET_BLOCK_PROCESS_RUNNING', {blockId, running: false});
        store.commit('SET_BLOCK_DATA_ERROR', {blockId, error: err.errInfo});
      }
    });
  },

  cancelBlockDataProcessing(store, {blockId}) {
    store.commit('SET_BLOCK_PROCESS_RUNNING', {blockId, running: false});
  },

  manualUpdateBlockData(store, {blockId, valid}) {
    if (!valid) {
      store.commit('UPDATE_BLOCK_DATA_VALIDITY', {blockId, validity: false});
    } else {
      runCommand({
        command: 'getBlockOutputs',
        args: [`"b${blockId}"`]
      }).then((result) => {
        store.dispatch('updateBlockOutputs', {blockId, outputInfo: result.$data});
        store.commit('UPDATE_BLOCK_DATA_VALIDITY', {blockId, validity: true});
      });
    }
  },

};
