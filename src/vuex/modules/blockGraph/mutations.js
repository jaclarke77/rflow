import Vue from 'vue';

export default {
  ADD_BLOCK(state, {id, block}) {
    Vue.set(state.blocks, id, block);
  },
  ADD_PIPE(state, {id, pipe}) {
    Vue.set(state.pipes, id, {
      ...pipe,
      order: 1,
      highlighted: false
    });
    if (pipe.start.block) {
      Vue.set(state.blocks[pipe.start.block].outsockets[pipe.start.socket].pipes, id, 0);
    }
    if (pipe.end.block) {
      Vue.set(state.blocks[pipe.end.block].insockets[pipe.end.socket].pipes, id, 0);
    }
  },
  UPDATE_BLOCK_DATA_VALIDITY(state, {blockId, validity}) {
    state.blocks[blockId].dataValid = validity;
  },
  SET_BLOCK_PROCESS_RUNNING(state, {blockId, running}) {
    state.blocks[blockId].processRunning = running;
  },
  SET_BLOCK_DATA_ERROR(state, {blockId, error}) {
    state.blocks[blockId].dataError = error;
  },
  SET_SOCKET_DATAFRAME(state, {blockId, socketId, dataframe}) {
    state.blocks[blockId].outsockets[socketId].dataframe = dataframe;
  },
  UPDATE_SOCKET_POS(state, {blockId, type, socketId, pos}) {
    state.blocks[blockId][type+'sockets'][socketId].ypos = pos;
  },
  UPDATE_PLUG_OFFSET(state, {blockId, socketId, pipeId, offset}) {
    state.blocks[blockId].outsockets[socketId].pipes[pipeId] = offset;
  },
  UPDATE_BLOCK_SIZE(state, {blockId, size}) {
    state.blocks[blockId].size = size;
  },
  UPDATE_BLOCK_RENDER_POS(state, {blockId, pos, relative}) {
    let block = state.blocks[blockId];
    if (relative) {
      block.renderPos = {
        x: block.renderPos.x + pos.x,
        y: block.renderPos.y + pos.y
      };
    }
    else {
      block.renderPos = pos;
    }
  },
  UPDATE_BLOCK_POS(state, {blockId, pos}) {
    state.blocks[blockId].pos = pos;
  },
  SET_BLOCK_SELECTED(state, {blockId, selected}) {
    state.blocks[blockId].selected = selected;
  },
  SET_BLOCK_DRAGGING(state, {blockId, dragging}) {
    state.blocks[blockId].dragging = dragging;
  },
  SET_BLOCK_ORDER(state, {blockId, order}) {
    state.blocks[blockId].order = order;
  },
  SET_PIPE_ORDER(state, {pipeId, order}) {
    state.pipes[pipeId].order = order;
  },
  RESET_PIPE_ORDERS(state) {
    for (var pipeId in state.pipes) {
      if (state.pipes.hasOwnProperty(pipeId) && state.pipes[pipeId].order !== 1) {
        state.pipes[pipeId].order = 1;
      }
    }
  },
  DETACH_PIPE(state, {pipeId, pipePlug, pos}) {
    var plug = state.pipes[pipeId][pipePlug],
        block = state.blocks[plug.block],
        blockPipes = block[(pipePlug === 'start') ? 'outsockets' : 'insockets'][plug.socket].pipes;

    Vue.delete(blockPipes, pipeId);

    plug.block = null;
    plug.socket = null;
    plug.pos = pos;
  },
  ATTACH_PIPE(state, {pipeId, blockId, socketType, socketId}) {
    var plug = state.pipes[pipeId][(socketType === 'outsockets') ? 'start' : 'end'],
        blockPipes = state.blocks[blockId][socketType][socketId].pipes;

    Vue.set(blockPipes, pipeId, 0);

    plug.block = blockId;
    plug.socket = socketId;
  },
  UPDATE_PLUG_POS(state, {pipeId, pipePlug, pos, relative}) {
    let plug = state.pipes[pipeId][pipePlug];

    plug.pos = (relative) ? {
      x: plug.pos.x + pos.x,
      y: plug.pos.y + pos.y
    } : pos;
  },
  REMOVE_PIPE(state, {pipeId}) {
    Vue.delete(state.pipes, pipeId);
  },
  SET_PIPE_HIGHLIGHT(state, {pipeId, highlighted}) {
    state.pipes[pipeId].highlighted = highlighted;
  },
  SET_SOCKET_FILTERS(state, filters) {
    state.socketFilters = filters;
  },
  REMOVE_BLOCK(state, {blockId}) {
    Vue.delete(state.blocks, blockId);
  },
  UPDATE_OPTIONS_DATA(state, {blockId, option, data}) {
    let optionsData = state.blocks[blockId].optionsData;
    if (optionsData[option]) optionsData[option] = data;
    else Vue.set(optionsData, option, data);
  },
  ADD_SOCKET(state, {blockId, socketType, id, socket, insertIndex}) {
    let block = state.blocks[blockId];
    Vue.set(block[socketType], id, socket);
    let order = block[socketType.slice(0,-1)+'Order']
    if (insertIndex !== -1) {
      order.splice(insertIndex, 0, id);
    }
    else {
      order.push(id);
    }
  },
  REMOVE_SOCKET(state, {blockId, socketType, id}) {
    let block = state.blocks[blockId];
    Vue.delete(block[socketType], id);
    let socketOrder = block[socketType.slice(0,-1)+'Order'];
    socketOrder.splice(socketOrder.indexOf(id), 1);
  },
  RENAME_SOCKET(state, {blockId, socketType, id, name}) {
    state.blocks[blockId][socketType][id].name = name;
  },
  UPDATE_SOCKET_OPTIONAL(state, {blockId, socketId, optional}) {
    state.blocks[blockId].insockets[socketId].optional = optional;
  },
  REORDER_SOCKETS(state, {blockId, socketType, order}) {
    state.blocks[blockId][socketType.slice(0,-1)+'Order'] = order;
  },
  UPDATE_BLOCK_MULTI_INPUTS(state, {blockId, multiId, multiInputs}) {
    Vue.set(state.blocks[blockId].multiInputs, multiId, multiInputs);
  },
  UPDATE_PERSIST_STATE(state, {blockId, stateId, data}) {
    let persistState = state.blocks[blockId].persistState;
    if (persistState[stateId]) persistState[stateId] = data;
    else Vue.set(persistState, stateId, data);
  },
  SET_BLOCK_GASP(state, {blockId, getter}) {
    state.blocks[blockId].getActiveSocketPositions = getter;
  },
  REMOVE_BLOCK_FROM_NOTES(state, {block}) {
    state.notes.forEach((note) => {
      let blockIndex = note.blocks.indexOf(block);
      if (blockIndex !== -1) note.blocks.splice(blockIndex, 1);
    });
  },
  ADD_NOTE(state, {blocks, preview = '', contents = ''}) {
    state.notes.push({
      blocks,
      preview,
      contents,
    });
  },
  REMOVE_NOTE(state, {note}) {
    state.notes.splice(state.notes.indexOf(note), 1);
  },
  UPDATE_NOTE_PREVIEW(state, {note, preview}) {
    note.preview = preview;
  },
  UPDATE_NOTE_CONTENTS(state, {note, contents}) {
    note.contents = contents;
  },
};
