import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

const state = {
  blocks: {},
  pipes: {},
  notes: [],
  socketFilters: {
    invalidBlocks: [],
    socketType: '',
    types: [],
  },
};

export default {
  state,
  mutations,
  actions,
  getters
};
