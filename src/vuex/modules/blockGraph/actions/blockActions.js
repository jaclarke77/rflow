import {round, tween, getBlocksPipes} from '../../../utils.js';

import {runCommand} from '../../../../utils/runcommand.js';

const genId = require('../../../../../shared/genid.js');

const {remote} = window.require('electron');

const ipcPromise = require('../../../../../shared/ipcPromise.js')

import isEqual from 'lodash/isEqual'

function stripInvalidChars(string) {
  return string.toLowerCase().replace(/[^a-z0-9]/g, '');
}

export default {

  // add/remove

  addBlock(store, {
    id = genId(),
    defId,
    //name, //
    pos,
    size = {
      height: 32,
      width: 160
    },
    //insockets = [], //
    //outsockets = [], //
    dataframes,
    customInputs = [],
    renamedOutputs = {},
    multiInputs = {},
    optionsData,
    persistState = {},
    dataValid = false,
    order = store.getters.topBlockOrder + 1
  }) {
    let blockDef = remote.getGlobal('blockDefs')[defId];

    function mapSockets(socketsArray, parentDataframe = null) {
      let sockets = {};
      let socketOrders = [];

      for (let socket of socketsArray) {
        let socketId = (parentDataframe ? (parentDataframe+'$') : '') + (socket.id ? socket.id : stripInvalidChars(socket.name));

        if (socket.multi) {
          let multiSocketsIds = multiInputs[socketId] || [socketId+'1'];
          multiInputs[socketId] = multiSocketsIds;
          let multiSocketsArray = multiSocketsIds.map((multiSocketId, i) => {
            return {
              id: multiSocketId,
              name: `${socket.name} ${i+1}`,
              type: socket.type,
              optional: (i === 0) ? socket.optional : true,
              multiId: socketId
            }
          })
          let {sockets: multiSockets, socketOrders: multiSocketOrder} = mapSockets(multiSocketsArray);
          sockets = {...sockets, ...multiSockets};
          socketOrders.push(...multiSocketOrder);
        }
        else {
          sockets[socketId] = {
            name: socket.name,
            type: socket.type,
            optional: socket.optional,
            multiId: socket.multiId,
            ypos: 0,
            pipes: {},
            parentDataframe
          };
          socketOrders.push(socketId);
        }
      }

      return {sockets, socketOrders};
    }

    let {sockets: insockets, socketOrders: insocketOrder} = mapSockets(
      (blockDef.inputs !== 'custom') ? blockDef.inputs : customInputs
    );

    let {sockets: outsockets, socketOrders: outsocketOrder} = mapSockets(
      (blockDef.outputs) ? ((blockDef.outputs.type === 'list') ? blockDef.outputs.items : [blockDef.outputs]) : []
    );
    if (dataframes) {
      for (let dataframeId of Object.keys(dataframes)) {
        let dataframeSockets = dataframes[dataframeId];
        outsockets[dataframeId].dataframe = dataframeSockets;
        let {sockets, socketOrders} = mapSockets(dataframeSockets, dataframeId);
        outsockets = {...outsockets, ...sockets};
        outsocketOrder.splice(outsocketOrder.indexOf(dataframeId)+1, 0, ...socketOrders);
      }
    }

    Object.entries(renamedOutputs).forEach(([id, name]) => {
      outsockets[id].name = name;
    })

    if (!optionsData) {
      optionsData = {};
      if (Array.isArray(blockDef.options)) {
        for (let option of blockDef.options) {
          switch (option.type) {
            case 'multiselect':
              optionsData[option.id] = []
              break;
            case 'boolean':
              optionsData[option.id] = false
              break;
            default:
              optionsData[option.id] = ''
          }
        }
      }
    }

    var block = {
      id,
      defId,
      name: blockDef.name,
      pos,
      renderPos: pos,
      size,
      insockets,
      insocketOrder,
      multiInputs,
      outsockets,
      outsocketOrder,
      optionsData,
      persistState,
      dataValid,
      order,
      processRunning: false,
      dataError: '',
      selected: false,
      dragging: false
    };

    store.commit('ADD_BLOCK', { id, block });

    store.commit('SET_UNSAVED');

    store.dispatch('updateBlockPos', {
      blockId: id,
      pos: pos
    });

    return id;
  },

  removeBlock(store, {blockId}) {
    let block = store.getters.blocks[blockId];

    for (let socketType of ['insockets', 'outsockets']) {
      for (let socketId of Object.keys(block[socketType])) {
        store.dispatch('ejectSocketPipes', {
          blockId,
          socketType,
          socketId
        })
      }
    }

    runCommand({
      command: 'removeBlock',
      args: [`"b${blockId}"`]
    })

    store.commit('REMOVE_BLOCK_FROM_NOTES', {block});
    store.commit('REMOVE_BLOCK', {blockId})
    store.commit('SET_UNSAVED');
  },

  // size/pos

  updateBlockSize(store, {blockId, size}) {
    store.commit('UPDATE_BLOCK_SIZE', {
      blockId,
      size
    })
  },

  updateBlockRenderPos(store, {blockId, pos, relative = false}) {
    // if no blockId supplied use all selected blocks
    var blocks = (blockId) ? [blockId] : store.getters.selectedBlocks;

    for (var i = 0, len = blocks.length; i < len; i++) {
      store.commit('UPDATE_BLOCK_RENDER_POS', {
        blockId: blocks[i],
        pos,
        relative,
        silent: true
      });
    }
  },

  updateBlockPos(store, {blockId, pos, relative = false}) {
    var blocks = (blockId) ? [blockId] : store.getters.selectedBlocks;

    for (var i = 0, len = blocks.length; i < len; i++) {
      let blockid = blocks[i],
          block = store.getters.blocks[blockid],
          blockPos = (relative) ? {
            x: block.renderPos.x + pos.x,
            y: block.renderPos.y + pos.y
          } : pos,
          roundedBlockPos = {
            x: round(blockPos.x),
            y: round(blockPos.y)
          };

      store.commit('UPDATE_BLOCK_POS', {
        blockId: blockid,
        pos: roundedBlockPos
      });

      store.commit('SET_UNSAVED');

      tween.start('blockpos-'+blockid, {
        type: 'expoOut',
        duration: 400,
        from: {...block.renderPos},
        to: roundedBlockPos
      }, ({value}) => {
        store.dispatch('updateBlockRenderPos', {
          blockId: blockid,
          pos: value
        })
      })

    }

    if (store.getters.topBlockOrder > store.getters.blockIds.length*10) {
      tween.allEnded(() => {
        store.dispatch('compactBlockOrder')
      })
    }
  },

  // selection

  setBlockSelected(store, {blockId, selected}) {
    store.commit('SET_BLOCK_SELECTED', {
      blockId,
      selected
    })
  },

  toggleBlockSelected(store, {blockId}) {
    var selected = !store.state.blocks[blockId].selected;
    store.dispatch('setBlockSelected', {
      blockId,
      selected
    })
  },

  // dragging

  startBlockDragging(store, {blockId}) {
    var draggingBlocks = (blockId) ? [blockId] : [...store.getters.selectedBlocks],
        blocks = store.getters.blocks,
        topBlockOrder = store.getters.topBlockOrder,
        draggingPipes = [];

    draggingBlocks.sort((a, b) => {
      return blocks[a].order - blocks[b].order;
    })

    for (var i = 0; i < draggingBlocks.length; i++) {
      let id = draggingBlocks[i];

      draggingPipes.push(...getBlocksPipes(store.getters.blocks[id]));

      tween.cancel('blockpos-'+id);

      store.commit('SET_BLOCK_DRAGGING', {
        blockId: id,
        dragging: true
      })

      store.commit('SET_BLOCK_ORDER', {
        blockId: id,
        order: (topBlockOrder + 1 + i)
      })
    }

    for (let pipe of draggingPipes) {
      store.commit('SET_PIPE_ORDER', {
        pipeId: pipe,
        order: (topBlockOrder + 1)
      })
    }

  },

  endBlockDragging(store, {blockId}) {
    var blocks = (blockId) ? [blockId] : [...store.getters.selectedBlocks];

    store.commit('RESET_PIPE_ORDERS');

    for (let block of blocks) {
      store.commit('SET_BLOCK_DRAGGING', {
        blockId: block,
        dragging: false
      })
    }
  },

  // ordering

  compactBlockOrder(store) {
    var blockIds = [...store.getters.blockIds],
        blocks = store.getters.blocks;

    blockIds.sort((a, b) => {
      return blocks[a].order - blocks[b].order;
    })

    for (var i = 0; i < blockIds.length; i++) {
      store.commit('SET_BLOCK_ORDER', {
        blockId: blockIds[i],
        order: (i + 1)
      })
    }
  },

  // inputs

  updateBlockInputs(store, {blockId, newInputs: newInputsArray}) {
    let block = store.getters.blocks[blockId],
        oldInputs = block.insockets,
        oldInputOrder = [...block.insocketOrder];

    let socketType = 'insockets';

    newInputsArray = newInputsArray.map(input => {
      if (!Array.isArray(input.type)) input.type = [input.type];
      return input;
    })

    let newInputOrder = newInputsArray.map(input => input.id);

    for (let i = 0, len = newInputOrder.length; i < len; i++) {
      let inputId = newInputOrder[i],
          newInput = newInputsArray[i];
      if (oldInputOrder.includes(inputId)) {
        let oldInput = oldInputs[inputId];
        if (!isEqual(newInput.type, oldInput.type)) {
          store.dispatch('removeSocket', {blockId, socketType, id: inputId})
          store.dispatch('addSocket', {blockId, socketType, id: inputId, name: newInput.name, type: newInput.type, optional: newInput.optional})
        }
        else if (newInput.name !== oldInput.name) {
          store.dispatch('renameSocket', {blockId, socketType, id: inputId, name: newInput.name})
        }
      }
      else {
        store.dispatch('addSocket', {blockId, socketType, id: inputId, name: newInput.name, type: newInput.type, optional: newInput.optional})
      }
    }

    for (let inputId of oldInputOrder) {
      if (!newInputOrder.includes(inputId)) {
        store.dispatch('removeSocket', {blockId, socketType, id: inputId})
      }
    }

    store.dispatch('reorderSockets', {blockId, socketType, order: newInputOrder});
  },

  updateMultiInput(store, {blockId, multiId}) {
    let block = store.getters.blocks[blockId];
    let allInputs = block.insockets;
    let multiInputIds = [...block.multiInputs[multiId]];
    let firstInput = allInputs[multiInputIds[0]];
    let multiName = firstInput.name.substr(0, firstInput.name.length-2);
    let multiType = firstInput.type;
    let multiOptional = firstInput.optional;
    let startIndex = block.insocketOrder.indexOf(multiInputIds[0]);

    let connectedInputIds = multiInputIds.filter((inputId) => {
      return Object.keys(allInputs[inputId].pipes).length;
    });

    if (multiInputIds.length === connectedInputIds.length) { // all sockets filled -> add new socket
      let newId = 1;
      while (multiInputIds.includes(multiId+newId)) {
        newId++;
      }
      store.dispatch('addSocket', {
        blockId,
        socketType: 'insockets',
        id: multiId+newId,
        name: `${multiName} ${multiInputIds.length+1}`,
        type: multiType,
        multiId,
        optional: true,
        insertIndex: startIndex+multiInputIds.length
      });
      multiInputIds.push(multiId+newId);
    }
    else { // too many sockets -> remove some + reorder + rename
      let i = 1;
      for (let inputId of connectedInputIds) { // rename connected sockets
        let input = allInputs[inputId];
        let optional = (i === 1) ? multiOptional : true;
        let newName = `${multiName} ${i++}`;
        if (input.name !== newName) {
          store.dispatch('renameSocket', {blockId, socketType: 'insockets', id: inputId, name: newName});
        }
        if (input.optional !== optional) {
          store.dispatch('updateSocketOptional', {blockId, socketId: inputId, optional});
        }
      }
      let emptyInputIds = multiInputIds.filter(id => !connectedInputIds.includes(id));
      if (emptyInputIds.length > 1) { // remove extra socket
        store.dispatch('removeSocket', {
          blockId, socketType: 'insockets', id: emptyInputIds.shift()
        });
      }
      let emptyInputId = emptyInputIds[0];

      // rename remaining empty socket
      let optional = (i === 1) ? multiOptional : true;
      let newName = `${multiName} ${i++}`;
      if (allInputs[emptyInputId].name !== newName) {
        store.dispatch('renameSocket', {blockId, socketType: 'insockets', id: emptyInputId, name: newName});
      }
      if (allInputs[emptyInputId].optional !== optional) {
        store.dispatch('updateSocketOptional', {blockId, socketId: emptyInputId, optional});
      }

      // move remaining empty socket
      let newOrder = [...block.insocketOrder];
      newOrder.splice(newOrder.indexOf(emptyInputId), 1);
      newOrder.splice(newOrder.indexOf(connectedInputIds[connectedInputIds.length-1])+1, 0, emptyInputId);
      if (!isEqual(block.insocketOrder, newOrder)) {
        store.dispatch('reorderSockets', {blockId, socketType: 'insockets', order: newOrder});
      }

      multiInputIds = newOrder.slice(startIndex, startIndex+(connectedInputIds.length+1));
    }

    store.commit('UPDATE_BLOCK_MULTI_INPUTS', {
      blockId, multiId, multiInputs: multiInputIds
    });
  },

  // outputs

  updateBlockOutputs(store, {blockId, outputInfo}) {
    const socketType = 'outsockets';
    let block = store.getters.blocks[blockId],
        oldOutputOrder = [...block.outsocketOrder];

    console.log(outputInfo);

    if (outputInfo.type === 'data.frame') {
      let dataframeId = oldOutputOrder.shift();
      let newOutputs = outputInfo.outputs.$data,
          newOutputNames = outputInfo.outputs.$names,
          newOutputOrder = [dataframeId],
          newDataframe = [];

      for (let outputName of newOutputNames) {
        let outputId = dataframeId+'$'+stripInvalidChars(outputName);
        let outputType = (newOutputs[outputName] === 'factor') ? 'character' : newOutputs[outputName];
        newOutputOrder.push(outputId);
        newDataframe.push({
          id: outputId,
          name: outputName,
          type: outputType
        });
        if (oldOutputOrder.includes(outputId)) {
          oldOutputOrder.splice(oldOutputOrder.indexOf(outputId), 1)
        }
        else {
          store.dispatch('addSocket', {
            blockId,
            socketType,
            id: outputId,
            name: outputName,
            type: outputType,
            parentDataframe: dataframeId
          })
        }
      }
      for (let oldOutputId of oldOutputOrder) {
        store.dispatch('removeSocket', {
          blockId,
          socketType,
          id: oldOutputId
        })
      }
      store.dispatch('reorderSockets', {blockId, socketType, order: newOutputOrder});
      store.commit('SET_SOCKET_DATAFRAME', {blockId, socketId: dataframeId, dataframe: newDataframe});
    }
  },

  // other

  getBlockTempDir(store, {blockId, blockDefId}) {
    let blockDef = remote.getGlobal('blockDefs')[blockDefId];

    if (blockDef.tempDir && blockDef.tempDir.required) {
      return ipcPromise.send('getBlockTempDir', blockId);
    }
    else {
      return null;
    }
  },

  setBlockGASP(store, {blockId, getter}) {
    store.commit('SET_BLOCK_GASP', {
      blockId,
      getter,
    })
  },

};
