export default {
  addNote(store) {
    const selectedBlockIds = store.getters.selectedBlocks;
    const selectedBlocks = selectedBlockIds.map(blockId => store.getters.blocks[blockId]);

    store.commit('ADD_NOTE', {blocks: selectedBlocks});
  },
}
