import {tween} from '../../../utils.js';

const genId = require('../../../../../shared/genid.js');

import mainStore from '../../../store.js';

export default {

  // add/remove

  addPipe(store, {id, pipe, fileLoading = false}) {
    if (!id) id = genId();

    var defaults = {
      block: null,
      socket: null,
      pos: {
        x: 0,
        y: 0
      }
    };

    pipe.start = Object.assign({}, defaults, pipe.start);
    pipe.end = Object.assign({}, defaults, pipe.end);

    store.commit('ADD_PIPE', { id, pipe });

    if (!fileLoading && pipe.end.socket) {
      let multiId = store.getters.blocks[pipe.end.block].insockets[pipe.end.socket].multiId;
      if (multiId) {
        store.dispatch('updateMultiInput', {blockId: pipe.end.block, multiId});
      }
    }

    store.commit('SET_UNSAVED');

    return id;
  },

  removePipe(store, {pipeId}) {
    tween.cancel('plugpos'+pipeId);
    store.commit('REMOVE_PIPE', {
      pipeId
    });
    store.commit('SET_UNSAVED');
  },

  // attach/detach

  attachPipe(store, {pipeId, blockId, socketType, socketId}) {
    store.commit('ATTACH_PIPE', {
      pipeId,
      blockId,
      socketType,
      socketId
    });
    store.commit('SET_UNSAVED');

    if (socketType === 'insockets') {
      let socket = store.getters.blocks[blockId].insockets[socketId];
      if (socket.multiId) {
        store.dispatch('updateMultiInput', {blockId, multiId: socket.multiId});
      }
    }
  },

  detachPipe(store, {pipeId, pipePlug}) {
    var pipe = store.getters.pipes[pipeId],
        blockId = pipe[pipePlug].block,
        block = store.getters.blocks[blockId],
        socket = block[(pipePlug === 'start') ? 'outsockets' : 'insockets'][pipe[pipePlug].socket],
        pos = {
          x: block.renderPos.x + ((pipePlug === 'start') ? block.size.width : 0),
          y: block.renderPos.y + socket.ypos + socket.pipes[pipeId]
        };

    store.commit('DETACH_PIPE', {
      pipeId,
      pipePlug,
      pos
    });
    store.commit('SET_UNSAVED');

    if (socket.multiId) {
      if (store.getters.globalDragging) {
        store.dispatch('updateMultiInput', {blockId, multiId: socket.multiId});
      }
      else {
        let unwatch = mainStore.watch(()=>store.getters.globalDragging, function(isDragging) {
          if (!isDragging) {
            unwatch();
            if (store.getters.blocks[blockId]) {
              store.dispatch('updateMultiInput', {blockId, multiId: socket.multiId});
            }
          }
        });
      }
    }
  },

  // ordering

  setPipeOrder(store, {pipeId, order}) {
    store.commit('SET_PIPE_ORDER', {
      pipeId,
      order
    });
  },

  // highlight

  setPipeHighlight(store, {pipeId, highlighted}) {
    store.commit('SET_PIPE_HIGHLIGHT', {
      pipeId,
      highlighted
    });
  },

  // plug pos/offset

  updatePlugPos(store, {pipeId, pipePlug, pos, relative = false}) {
    store.commit('UPDATE_PLUG_POS', {
      pipeId,
      pipePlug,
      pos,
      relative,
      silent: true
    });
    store.commit('SET_UNSAVED');
  },

  updatePlugOffset(store, {blockId, socketId, pipeId, offset}) {
    store.commit('UPDATE_PLUG_OFFSET', {
      blockId,
      socketId,
      pipeId,
      offset
    });
  },

};
