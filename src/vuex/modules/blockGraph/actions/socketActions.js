import {tween} from '../../../utils.js';

export default {

  // add/remove

  addSocket(store, {blockId, socketType, id, name, type, multiId, optional = false, parentDataframe = null, insertIndex = -1}) {
    store.commit('ADD_SOCKET', {
      blockId, socketType, id, insertIndex,
      socket: {
        name,
        type,
        optional,
        multiId,
        ypos: 0,
        pipes: {},
        parentDataframe
      }
    });
    store.commit('SET_UNSAVED');
  },

  removeSocket(store, {blockId, socketType, id}) {
    store.dispatch('ejectSocketPipes', {blockId, socketType, socketId: id});
    store.commit('REMOVE_SOCKET', {
      blockId, socketType, id
    });
    store.commit('SET_UNSAVED');
  },

  // rename/reorder

  renameSocket(store, {blockId, socketType, id, name}) {
    store.commit('RENAME_SOCKET', {blockId, socketType, id, name});
    store.commit('SET_UNSAVED');
  },

  reorderSockets(store, {blockId, socketType, order}) {
    store.commit('REORDER_SOCKETS', {blockId, socketType, order});
    store.commit('SET_UNSAVED');
  },

  updateSocketOptional(store, {blockId, socketId, optional}) {
    store.commit('UPDATE_SOCKET_OPTIONAL', {blockId, socketId, optional});
  },

  // position

  updateSocketPosition(store, {blockId, type, socketId, pos}) {
    store.commit('UPDATE_SOCKET_POS', {
      blockId,
      type,
      socketId,
      pos
    });
  },

  // filters (socket highlighting)

  setSocketFilters(store, filters) {
    store.commit('SET_SOCKET_FILTERS', filters);
  },

  clearSocketFilters(store) {
    store.commit('SET_SOCKET_FILTERS', {
      invalidBlocks: [],
      socketType: '',
      types: [],
    })
  },

  // eject pipes

  ejectPipe(store, {pipeId, pipePlug}) {
    const pipe = store.getters.pipes[pipeId];

    if (pipe[pipePlug].block) {
      store.dispatch('detachPipe', {pipeId, pipePlug});
    }

    if (!pipe.start.block && !pipe.end.block) {
      store.dispatch('removePipe', {pipeId});
    }
    else {
      tween.start('plugpos'+pipeId, {
        type: 'expoOut',
        duration: 300,
        from: pipe[pipePlug].pos,
        to: {
          x: pipe[pipePlug].pos.x + (pipePlug === 'end' ? -30 : 30),
          y: pipe[pipePlug].pos.y
        }
      }, ({value}) => {
        store.dispatch('updatePlugPos', {
          pipeId: pipeId,
          pipePlug,
          pos: value
        });
      });
    }
  },

  ejectSocketPipes(store, {blockId, socketType, socketId}) {
    let block = store.getters.blocks[blockId],
        socket = block[socketType][socketId],
        pipePlug = (socketType === 'insockets') ? 'end' : 'start';

    Object.keys(socket.pipes).forEach(pipeId => store.dispatch('ejectPipe', {
      pipeId,
      pipePlug,
    }))
  },

};
