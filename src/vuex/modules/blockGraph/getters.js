export default {

  blockBoundingBox(state) {
    var left = 0,
      right = 0,
      top = 0,
      bottom = 0,
      blocks = state.blocks,
      first = true;

    for (var key in blocks) {
      if (blocks.hasOwnProperty(key)) {
        var block = blocks[key];
        if (first || block.pos.x < left) left = block.pos.x;
        if (first || block.pos.y < top) top = block.pos.y;
        if (first || (block.pos.x+block.size.width) > right) right = (block.pos.x+block.size.width);
        if (first || (block.pos.y+block.size.height) > bottom) bottom = (block.pos.y+block.size.height);
        first = false;
      }
    }

    return {
      left: left,
      top: top,
      width: right - left,
      height: bottom - top
    };
  },

  blocks(state) {
    return state.blocks;
  },

  pipes(state) {
    return state.pipes;
  },

  notes(state) {
    return state.notes;
  },

  blockIds(state) {
    return Object.keys(state.blocks);
  },

  selectedBlocks(state) {
    var blocks = state.blocks,
        selectedBlocks = [];

    for (var id in blocks) {
      if (blocks.hasOwnProperty(id) && blocks[id].selected) {
        selectedBlocks.push(id);
      }
    }

    return selectedBlocks;
  },

  unselectedBlocks(state) {
    var blocks = state.blocks,
        unselectedBlocks = [];

    for (var id in blocks) {
      if (blocks.hasOwnProperty(id) && !blocks[id].selected) {
        unselectedBlocks.push(id);
      }
    }
    return unselectedBlocks;
  },

  topBlockOrder(state) {
    var topBlock = 0,
        blocks = state.blocks;

    for (var id in blocks) {
      if (blocks.hasOwnProperty(id) && blocks[id].order > topBlock) {
        topBlock = blocks[id].order;
      }
    }

    return topBlock;
  },

  noPipesHighlighted(state) {
    var pipes = state.pipes,
        pipeIds = Object.keys(pipes);

    for (let id of pipeIds) {
      if (pipes[id].highlighted) return false;
    }

    return true;
  },

  socketFilters(state) {
    return state.socketFilters;
  },

};
