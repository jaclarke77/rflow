import {round} from '../../utils.js';

export default {

  canvasMoveTo(store, {pos, relative = false, silent = false}) {
    pos.x = Math.round(pos.x);
    pos.y = Math.round(pos.y);

    if (relative) {
      pos.x += store.state.pos.x;
      pos.y += store.state.pos.y;
    }
    // check pos is within bounds
    var canvasSize = store.getters.canvasSize,
        screenSize = store.state.screenSize;

    if (canvasSize.width + pos.x > screenSize.width) pos.x = (pos.x > 0) ? 0 : pos.x;
    else pos.x = screenSize.width - canvasSize.width;

    if (canvasSize.height + pos.y > screenSize.height) pos.y = (pos.y > 0) ? 0 : pos.y;
    else pos.y = screenSize.height - canvasSize.height;

    store.commit('UPDATE_CANVAS_POS', {pos, silent});
  },

  canvasMoveToCenter(store) {
    var center = {
      x: Math.round((store.state.screenSize.width - store.getters.canvasSize.width) / 2),
      y: Math.round((store.state.screenSize.height - store.getters.canvasSize.height) / 2)
    };

    store.dispatch('canvasMoveTo', {pos: center});
  },

  updateScreenSize(store, {size}) {
    store.commit('UPDATE_SCREEN_SIZE', {size, silent: true});
  }

};
