import {round} from '../../utils.js';

export default {

  canvasOrigin(state, getters) {
    var canvasSize = getters.canvasSize,
        blockBoundingBox = getters.blockBoundingBox;

    return {
      x: round( ((canvasSize.width - blockBoundingBox.width) / 2) - blockBoundingBox.left ),
      y: round( ((canvasSize.height - blockBoundingBox.height) / 2) - blockBoundingBox.top )
    };
  },

  canvasSize(state, getters) {
    var screenWidth = round(state.screenSize.width, 'up'),
        screenHeight = round(state.screenSize.height, 'up'),
        boundingBox = getters.blockBoundingBox;

    return {
      width: boundingBox.width + screenWidth * 2,
      height: boundingBox.height + screenHeight * 2
    };
  },

  canvasPos(state) {
    return state.pos;
  },

  screenSize(state) {
    return state.screenSize;
  }

};
