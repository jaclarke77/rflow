import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

const state = {
  pos: {
    x: 0,
    y: 0
  },
  screenSize: {
    width: 600,
    height: 400
  },
  overscroll: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
