export default {
  UPDATE_CANVAS_POS(state, {pos}) {
    state.pos = pos;
  },
  UPDATE_SCREEN_SIZE(state, {size}) {
    state.screenSize = size;
  }
};
