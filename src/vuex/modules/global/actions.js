export default {

  setGlobalDragging(store, dragging = false) {
    store.commit('SET_GLOBAL_DRAGGING', {
      dragging
    });
  },

};
