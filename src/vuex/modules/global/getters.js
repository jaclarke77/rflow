export default {

  globalDragging(state) {
    return state.dragging;
  },

  filename(state) {
    let filename = state.filepath.split(/[/\\]/).pop();
    let filenameParts = filename.split('.');
    if (filenameParts.length > 1) filenameParts.pop();
    return filenameParts.join('.');
  },

  unsaved(state) {
    return state.unsaved;
  },

  isFileLoading(state) {
    return state.fileLoading;
  }

};
