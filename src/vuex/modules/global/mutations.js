export default {
  SET_GLOBAL_DRAGGING(state, {dragging}) {
    state.dragging = dragging;
  },
  SET_UNSAVED(state, unsaved=true) {
    state.unsaved = unsaved;
  },
  SET_FILEPATH(state, filepath) {
    state.filepath = filepath;
  },
  SET_FILE_LOADING(state, isLoading) {
    state.fileLoading = isLoading;
  }
};
