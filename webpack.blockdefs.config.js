const path = require('path');
const webpack = require('webpack');
const glob = require('glob');

const loaders = [
  {
    test: /\.vue$/,
    loader: 'vue'
  },
  {
    test: /\.js$/,
    loader: 'babel',
    include: [
      path.resolve(__dirname, "block_defs"),
    ]
  },
  {
    test: /\.json$/,
    loader: 'json'
  },
  {
    test: /\.html$/,
    loader: 'vue-html'
  },
  {
    test: /\.svg$/,
    loader: 'svg-sprite'
  },
  {
    test: /\.(png|jpg|gif)$/,
    loader: 'url',
    query: {
      limit: 10000,
      name: '[name].[ext]?[hash]'
    }
  }
];

let entriesList = glob.sync('./*/main.vue', {
    cwd: './block_defs'
  }),
  entries = {};
for (let entry of entriesList) {
  entries[entry.slice(0,-9)] = entry;
}

module.exports = {
  context: path.resolve(__dirname, './block_defs'),
  entry: entries,
  output: {
    path: path.resolve(__dirname, './block_defs'),
    filename: '[name]/main.js',
    library: 'customOpts',
    libraryTarget: 'commonjs2'
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules'),
  },
  module: {
    loaders: loaders
  },
  vue: {
    postcss: [
      require('autoprefixer')({
        browsers: ['last 2 versions']
      })
    ]
  }
};

if (process.env.NODE_ENV === 'production') {
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin()
  ]);
}
