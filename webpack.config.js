const path = require('path');
const webpack = require('webpack');

const loaders = [
  {
    test: /\.vue$/,
    loader: 'vue'
  },
  {
    test: /\.js$/,
    loader: 'babel',
    include: [
      path.resolve(__dirname, "src"),
      path.resolve(__dirname, "shared"),
      path.resolve(__dirname, "node_modules/quill"),
    ]
  },
  {
    test: /\.json$/,
    loader: 'json'
  },
  {
    test: /\.html$/,
    loader: 'vue-html'
  },
  {
    test: /.*quill.*\.svg$/,
    loader: 'html?minimize=true'
  },
  {
    test: /.*src.*\.svg$/,
    loader: 'svg-sprite'
  },
  {
    test: /\.(png|jpg|gif)$/,
    loader: 'url',
    query: {
      limit: 10000,
      name: '[name].[ext]?[hash]'
    }
  }
];

module.exports = {
  target: 'electron-renderer',
  entry: {
    vendor: ['vue'],
    app: './src/main.js',
    startup: './src/startup/startup.js'
  },
  output: {
    path: path.resolve(__dirname, './ui'),
    publicPath: '/ui/',
    filename: '[name].js'
  },
  resolve: {
    alias: {
      // 'parchment': path.resolve(__dirname, 'node_modules/parchment/src/parchment.ts'),
      'quill$': path.resolve(__dirname, 'node_modules/quill/quill.js'),
    },
  },
  resolveLoader: {
    root: path.join(__dirname, 'node_modules'),
  },
  module: {
    loaders: loaders
  },
  vue: {
    postcss: [
      require('autoprefixer')({
        browsers: ['last 2 versions']
      })
    ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
  devtool: '#eval-source-map'
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';
  // http://vuejs.github.io/vue-loader/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      minChunks: Infinity,
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      }
    }),
    new webpack.optimize.OccurenceOrderPlugin()
  ]);
}
