function(block, importData) {
  na.pad <- function(x,len){
    x[1:len]
  }
  
  depthIndex <- 0L
  for(depth in importData$data) {
    isLast <- depthIndex + 1 == length(importData$data)
    childIndex <- 1L
    
    dataframeList <- purrr::map(depth$dataFrames, function(dfStructure) {
      dataframe <- list()
      
      for (col in dfStructure) {
        if (class(col) == "character") {
          dataframe <- append(dataframe, read.csv(col, header = T));
        }
        else if (is.null(col)) {
          dataframe <- append(dataframe, c(NA));
        }
        else {
          dataframe <- append(dataframe, col[1]);
        }
      }
      childCount <- length(dataframe[[1]])
      
      dataframe <- purrr::map2(dataframe, depth$classes, function(col, type) {
        return(
          switch(type, "character" = as.character(col), "numeric" = as.numeric(col), "integer" = as.integer(col))
        )
      })
      
      maxlen <- max(sapply(dataframe, length))
      dataframe <- data.frame(lapply(dataframe, na.pad, len = maxlen), stringsAsFactors = F)
      
      colnames(dataframe) <- depth$colNames
      
      if (!isLast) {
        attr(dataframe, "childIndex") <- childIndex
        attr(dataframe, "childCount") <- childCount
        childIndex <<- childIndex + childCount
      }
      
      return( dataframe )
    })
    
    if (depthIndex != 0) {
      attr(dataframeList, "parent") <- paste(importData$blockId, depthIndex - 1, sep = '.')
    }
    blockName <- ifelse(isLast, importData$blockId, paste(importData$blockId, depthIndex, sep = '.'))
    assign(blockName, dataframeList, envir = blockData)
    depthIndex <- depthIndex + 1L
  }
}
