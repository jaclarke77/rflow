import isEqual from 'lodash/isEqual'
import some from 'lodash/some'
import uniq from 'lodash/uniq'
import zip from 'lodash/zip'
import unzip from 'lodash/unzip'
import flatten from 'lodash/flatten'
// import findIndex from 'lodash/findIndex'

const hasDuplicates = (array) => some(array, (el, i) => array.indexOf(el) !== i);

export function createImportStructure(basePath, fileStructures) {
  const importStructure = [];

  function getDepth(depthIndex) {
    if (!importStructure[depthIndex]) {
      importStructure[depthIndex] = {
      selected: null,
      selectedFlat: [],
      folder: {
        folder: true,
        colNames: [''],
        classes: [],
        paths: [],
      },
      fileStructures: []}
    }
    return importStructure[depthIndex];
  }

  function addFolder(depth, folderPath) {
    const folderPaths = depth.folder.paths;
    if (!folderPaths.includes(folderPath)) folderPaths.push(folderPath);
  }

  function addFile(depth, filePath, fileStructure) {
    const fileStructures = depth.fileStructures;
    let matchedStucture, i = 0;
    while (!matchedStucture && i < fileStructures.length) {
      let struct = fileStructures[i++];
      if (isEqual(struct.colNames, fileStructure.colNames)) matchedStucture = struct;
    }
    if (!matchedStucture) {
      let colNames = Array.isArray(fileStructure.colNames) ? fileStructure.colNames : [fileStructure.colNames];
      matchedStucture = {
        colNames: colNames,
        classes: colNames
          .map(colName => fileStructure.classes[colName])
          .map(type => ['integer', 'numeric', 'double'].includes(type) ? 'numeric' : 'character'),
        sample: zip(...colNames.map(colName => fileStructure.sample.$data[colName])),
        paths: [],
      };
      fileStructures.push(matchedStucture);
    }

    matchedStucture.paths.push(filePath);
  }

  for (let [filePath, fileStructure] of Object.entries(fileStructures)) {
    let shortPath = filePath.substring(basePath.length+1);
    let pathParts = shortPath.split('/').slice(0, -1);

    addFile(getDepth(pathParts.length), shortPath, fileStructure.$data);

    while (pathParts.length) {
      let depth = getDepth(pathParts.length - 1);
      addFolder(depth, pathParts.join('/'));
      pathParts.pop();
    }
  }

  for (let depth of importStructure) {
    for (let fileStructure of depth.fileStructures) {
      let parentPaths = fileStructure.paths.map(path => path.split('/').slice(0, -1).join('/'));
      if (!hasDuplicates(parentPaths)) {
        fileStructure.flat = true;
      } else {
        fileStructure.innerStructure = {
          inner: true,
          colNames: fileStructure.colNames,
          classes: fileStructure.classes,
          sample: fileStructure.sample,
        }
        fileStructure.colNames = ['']
        fileStructure.classes = []
        fileStructure.sample = null
      }
    }

    depth.fileStructures.sort((a, b) => !!a.flat - !!b.flat);

    if (depth.folder.paths.length) depth.selected = depth.folder;
    else if (depth.fileStructures[0]) {
      if (!depth.fileStructures[0].flat) depth.selected = depth.fileStructures[0];
      else depth.selectedFlat.push(depth.fileStructures[0]);
    }
  }

  return importStructure;
}

export function formatData(data) {
  const uniqData = uniq(data);
  const nums = uniqData.map(parseFloat);

  if ( some(nums, (num, i) => isNaN(num) || !/^[0-9-.]+$/.test(uniqData[i])) ) {
    return {
      class: 'character',
      data: uniqData.sort(),
    }
  }

  nums.sort((a,b) => a-b);

  // if ( some(nums, num => !Number.isInteger(num)) ) {
    return {
      class: 'numeric',
      data: nums,
    }
  // }

  // return {
  //   class: 'integer',
  //   data: nums,
  // }
}

export function isColNameValid(colName) {
  return colName.trim() !== '';
}

function coerceToClass(val, type) {
  switch (type) {
    case 'character':
      return val;
    case 'integer':
      return parseInt(val);
    case 'numeric':
      return parseFloat(val);
  }
}

export function createRImportData(basePath, importStructure) {
  const importData = [];

  function createDepth(depth, colNames, classes) {
    importData[depth] = {
      colNames,
      classes,
      dataFrames: {},
    }
  }

  function addToDataFrame(depth, parentPath, index, data, isFlat = false) {
    const dfs = importData[depth].dataFrames;
    if (!dfs[parentPath]) dfs[parentPath] = [];
    if (isFlat) {
      dfs[parentPath][index] = [basePath, parentPath, data].filter(x => x).join('/');
    } else {
      if (!dfs[parentPath][index]) dfs[parentPath][index] = [];
      if (!dfs[parentPath][index].includes(data)) dfs[parentPath][index].push(data);
    }
  }

  let selectedStructures = [];
  let i = 0, depth = importStructure[i++];
  while(depth) {
    const currentDepth = i - 1;
    const structures = [depth.selected, ...depth.selectedFlat].filter(struct => struct);

    selectedStructures.push(structures);
    createDepth(
      currentDepth, 
      flatten(structures.map(struct => struct.colNames)), 
      flatten(structures.map(struct => struct.classes))
    );

    structures.forEach((structure, index) => {
      if (structure.folder) return;

      if (structure.innerStructure) {
        selectedStructures.push([structure.innerStructure]);
        createDepth(currentDepth + 1, structure.innerStructure.colNames, structure.innerStructure.classes);
      }

      for (let path of structure.paths) {
        let pathParts = path.split('/');

        let data = pathParts.pop();
        addToDataFrame(currentDepth, pathParts.join('/'), index, data, structure.flat);

        if (structure.innerStructure) addToDataFrame(currentDepth+1, path, 0, '', true);

        while (pathParts.length) {
          let data = pathParts.pop();
          addToDataFrame(pathParts.length, pathParts.join('/'), 0, data);
        }
      }
    });

    depth = (!structures[0].innerStructure) ? importStructure[i++] : null;
  }

  let depthOrder = [''];
  importData.forEach((depth, index) => {
    const structures = selectedStructures[index];
    const newDepthOrder = [];
    const dataframes = [];

    for (let parentPath of depthOrder) {
      let dataframe = depth.dataFrames[parentPath];
      let newDataframe = [];

      structures.forEach((structure, i) => {
        let col = dataframe[i];
        if (!col) {
          newDataframe.push(...Array(structure.colNames.length).fill([]));
          return;
        }
        if (structure.flat || structure.inner) {
          newDataframe.push(col);
          return;
        }
        const colType = structure.classes[0];
        let formattedCol = col.map((val) => {
          if (!structure.folder) {
            val = val.split('.').slice(0,-1).join('.');
          }
          return coerceToClass(val, colType);
        });

        const cols = zip(col, formattedCol);
        cols.sort((a,b) => {
          if (a[1] < b[1]) return -1;
          if (a[1] > b[1]) return 1;
          return 0;
        })
        let [orderedCol, orderedFormattedCol] = unzip(cols);

        newDataframe.push([orderedFormattedCol]);
        newDepthOrder.push(...orderedCol.map(col => parentPath ? `${parentPath}/${col}` : col));
      });

      dataframes.push(newDataframe);
    }

    depth.dataFrames = dataframes;
    depthOrder = newDepthOrder;
  });

  return importData;
}
