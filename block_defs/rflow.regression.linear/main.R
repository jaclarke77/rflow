function(inputs, options, inputNames) {
  forceOrigin <- options[["forceOrigin"]]

  if (forceOrigin) {
    reg <- lm(inputs[["y"]] ~ inputs[["x"]] + 0)
  } else {
    reg <- lm(inputs[["y"]] ~ inputs[["x"]])
  }

  gradient <- ifelse(forceOrigin, reg$coefficients[[1]], reg$coefficients[[2]])
  intercept <- ifelse(forceOrigin, 0, reg$coefficients[[1]])

  return(list(gradient = gradient, intercept = intercept, trendline = ggplot2::geom_abline(slope = gradient, intercept = intercept)))
}
