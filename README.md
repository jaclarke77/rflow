# R ← Flow_

Prototype visual programming interface for data analysis using R

![Screenshot 1](screenshot_1.png)
![Screenshot 1](screenshot_2.png)

## Setup

``` bash
# install dependencies
yarn install

# build js for production (with minification, etc.)
npm run build

# start app
npm run start

# pack app and build installer
npm run dist
```

## Development

``` bash
# run dev server for main app (with hot reload)
npm run dev

# build blocks
npm run build:blocks:watch

# start app
npm run start:dev
